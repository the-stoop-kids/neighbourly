[Return to README](../README.md)

# Wireframes

## Main Page

![Not Logged In](./wireframes/mainpage_notloggedin.png)

![Logged In](./wireframes/mainpage_loggedin.png)

### Bills

![Bills](./wireframes/bills.png)

### Events

![Events](./wireframes/events.png)
