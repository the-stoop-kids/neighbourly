[Return to README](../README.md)

# Data Schema

## Overview

![Overview](./images/overview.png)

## Detail View

### Bills

![Bills](./images/bills.png)

### Community

![Community](./images/community.png)

### Events

![Events](./images/events.png)

### Maintenance

![Maintenance](./images/maintenance.png)

### Properties

![Properties](./images/properties.png)

### Reviews

![Reviews](./images/reviews.png)

### Users

![Users](./images/users.png)
