[Return to README](../README.md)

# Table of Contents

- [Group Contributions]
- [Individual Contributions]
    - [Erika Linden]
    - [Jaron Laquindanum]
    - [Benjamin Stults]

## Group Contribution

- DB Migration files for tables
- Create and Get endpoints for Events
- Auth endpoints modification and testing
- Associative Tables
- Issue Templates
- CSS
- README

## Erika Linden

### Backend
- CRUD endpoints for Properties
- CRUD endpoints for Users
- Update, Get by ID, and Delete Endpoints for Events
- MaintenanceForm
- MaintenanceEdit
- PropertiesList
- EventForm
- EventEdit

### Backend
- LoginForm
- CommunitiesList
- CreateCommunityForm
- CommunityEditForm

### Unit Tests
- Reviews Get all and Create

## Jaron Laquindanum

### Backend
- CRUD endpoints for Maintenance
- CRUD endpoints for Communities

### FrontEnd
- HomePage
- MaintenanceList
- EventsList

### Unit Tests

## Benjamin Stults

### Backend
- CRUD endpoints for Bills
- CRUD endpoints for Reviews

### Frontend
- SignupForm
- UsersList
- User Profile
- UserEditForm
- ResetPassword
- BillsList
- BillForm
- BillEditForm
- Sidenav
- ReviewsList
- ReviewForm
- ReviewEditForm

### Unit Tests
- Events Get all and Create
