[Return to README](../README.md)

# Table of Contents

-   [Auth Endpoints](#auth-endpoints)
    -   [Example Response/Request Data](#requestresponse-data)
-   [Users Table and Endpoints](#user-table-and-endpoints)
    -   [Example Response/Request Data](#requestresponse-data-1)
-   [Events Table and Endpoints](#events-table-and-endpoints)
    -   [Example Response/Request Data](#requestresponse-data-2)
-   [Communities Table and Endpoints](#community-table-and-endpoints)
    -   [Example Response/Request Data](#requestresponse-data-3)
-   [Properties Table and Endpoints](#property-table-and-endpoints)
    -   [Example Response/Request Data](#requestresponse-data-4)
-   [Maintenance Table and Endpoints](#maintenance-table-and-endpoints)
    -   [Example Response/Request Data](#requestresponse-data-5)
-   [Bills Table and Endpoints](#bills-table-and-endpoints)
    -   [Example Response/Request Data](#requestresponse-data-6)
-   [Reviews Table and Endpoints](#review-table-and-endpoints)
    -   [Example Response/Request Data](#requestresponse-data-7)

## Neighbourly APIs

### Auth Endpoints

| ACTION       | METHOD | URL                                         |
| ------------ | ------ | ------------------------------------------- |
| Signup       | POST   | http://localhost:8000/api/users/            |
| Signin       | POST   | http://localhost:8000/api/users/            |
| Authenticate | GET    | http://localhost:8000/api/users/<<int:id>>/ |
| Signout      | DELETE | http://localhost:8000/api/users/<<int:id>>/ |

### Request/Response Data:

##### POST `/api/auth/signup/`:

-   Creates a user

Request:

```
{
  "username": "string",
  "password": "string",
  "first_name": "string",
  "last_name": "string",
  "picture_url": "string",
  "email": "string",
  "phone_number": 0,
  "role": 0
}
```

Response:

```
{
  "id": 0,
  "username": "string",
  "first_name": "string",
  "last_name": "string",
  "picture_url": "string",
  "email": "string",
  "phone_number": 0,
  "role": 0,
  "created_on": "2024-06-24T00:12:57.249Z"
}
```

#### POST `/api/auth/signin/`

-   Sign in a user

Request:

```
{
  "username": "string",
  "password": "string"
}
```

Response:

```
{
  "id": 0,
  "username": "string",
  "first_name": "string",
  "last_name": "string",
  "picture_url": "string",
  "email": "string",
  "phone_number": 0,
  "role": 0,
  "created_on": "2024-06-24T00:12:57.249Z"
}
```

#### GET `/api/auth/authenticate`

-   Validate a user

Request:

```
fast_api_token which looks like:
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c
```

Response:

```
{
  "id": 0,
  "username": "string"
}
```

#### DELETE `/api/auth/signout/`

-   Signout a user

Response:

```
200: message
```

### User Table and Endpoints:

| ACTION         | METHOD | URL                                                        |
| -------------- | ------ | ---------------------------------------------------------- |
| List Users     | GET    | http://localhost:8000/api/users/                           |
| User Detail    | GET    | http://localhost:8000/api/users/<<int:id>>/                |
| Update User    | PUT    | http://localhost:8000/api/users/<<int:id>>/                |
| Delete User    | DELETE | http://localhost:8000/api/users/<<int:id>>/                |
| Reset Password | PUT    | http://localhost:8000/api/users/<<int:id>>/reset-password/ |

### Request/Response Data:

#### PUT Update a user by <<int:id>>:

Request:

```
user_id
{
  "username": "string",
  "first_name": "string",
  "last_name": "string",
  "picture_url": "string",
  "email": "string",
  "phone_number": 0,
  "role": 0
}
```

Response:

```
{
  "id": 0,
  "username": "string",
  "first_name": "string",
  "last_name": "string",
  "picture_url": "string",
  "email": "string",
  "phone_number": 0,
  "role": 0,
  "created_on": "2024-06-24T00:23:28.387Z"
}
```

#### GET List Users or a User's details by <<int:id>>:

Response:

```
{
    "users": [
      {
        "id": 0,
        "username": "string",
        "first_name": "string",
        "last_name": "string",
        "picture_url": "string",
        "email": "string",
        "phone_number": 0,
        "role": 0,
        "created_on": "2024-06-24T00:23:28.387Z"
    },
      {
        "id": 1,
        "username": "string",
        "first_name": "string",
        "last_name": "string",
        "picture_url": "string",
        "email": "string",
        "phone_number": 0,
        "role": 0,
        "created_on": "2024-06-24T00:23:28.387Z"
    },
  ]
}
```

#### PUT Reset a user's password by <<int:id>>:

Request:

```
user_id
{
  "username": "string",
  "password": "string",
  "password2": "string"
}
```

Response:

```
{
  "id": 0,
  "username": "string",
  "first_name": "string",
  "last_name": "string",
  "picture_url": "string",
  "email": "string",
  "phone_number": 0,
  "role": 0,
  "created_on": "2024-06-24T00:26:13.017Z"
}
```

#### Delete a User by <<int:id>>:

Response:

```
{
	"deleted": "User deleted."
}
```

### EVENTS Table and Endpoints:

| ACTION                  | METHOD | URL                                          |
| ----------------------- | ------ | -------------------------------------------- |
| List events             | GET    | http://localhost:8000/api/events/            |
| Create an event         | POST   | http://localhost:8000/api/events/            |
| Get a specific event    | GET    | http://localhost:8000/api/events/<<int:id>>/ |
| Update a specific event | PUT    | http://localhost:8000/api/events/<<int:id>>/ |
| Delete a specific event | DELETE | http://localhost:8000/api/events/<<int:id>>/ |

### Request/Response DATA:

JSON Body to send data:

-   Create (POST) an event:

```
{
  "title": "string",
  "description": "string",
  "location": "string",
  "event_date": "2024-06-24",
  "maker": 0,
  "category": 0
}
```

-   Update (PUT) an event:

```
{
  "location": "string",
  "event_date": "2024-06-24"
}
```

JSON Body returned data:

-   List (GET) events by maker.id or (GET/PUT) event detail by <<int:id>> (event.id):

```
{
	"events": [
      {
        "id": 0,
        "title": "string",
        "description": "string",
        "location": "string",
        "event_date": "2024-06-24",
        "maker": 0,
        "category": 0,
        "created_on": "2024-06-24T00:32:18.051Z"
    },
  ]
}
```

-   Delete an event by <<int:id>>:

```
{
	"deleted": "Deleted event."
}
```

#### Community Table and Endpoints:

| ACTION             | METHOD | URL                                               |
| ------------------ | ------ | ------------------------------------------------- |
| List communities   | GET    | http://localhost:8000/api/communities/            |
| Create a community | POST   | http://localhost:8000/api/communities/            |
| Community detail   | GET    | http://localhost:8000/api/communities/<<int:id>>/ |
| Update a community | PUT    | http://localhost:8000/api/communities/<<int:id>>/ |
| Delete a community | DELETE | http://localhost:8000/api/communities/<<int:id>>/ |

#### Request/Response DATA

JSON Body to send data:

-   Create (POST) a community:

```
{
  "title": "string",
  "owner": 0,
  "description": "string",
  "city": "string",
  "state": "string",
  "zipcode": 0,
  "rating": 0
}
```

-   Update (PUT) a community by <<int:id>>:

```
{
  "title": "string",
  "owner": 0,
  "description": "string",
  "city": "string",
  "state": "string",
  "zipcode": 0,
  "rating": 0
}
```

JSON Body returned Data:

-   List (GET) community or (GET/PUT) community detail by <<int:id>>:

```
{
  "communities": [
      {
        "id": 0,
        "title": "string",
        "first_name": "string",
        "last_name": "string",
        "description": "string",
        "city": "string",
        "state": "string",
        "zipcode": 0,
        "rating": 0,
        "created_on": "2024-06-24T00:36:46.409Z"
    },
  ]
}
```

-   Delete a community by <<int:id>>:

```
{
	"deleted": "Deleted community"
}
```

### PROPERTY Table and Endpoints:

| ACTION                    | METHOD | URL                                              |
| ------------------------- | ------ | ------------------------------------------------ |
| List properties           | GET    | http://localhost:8000/api/properties/            |
| Create a new property     | POST   | http://localhost:8000/api/properties/            |
| Get details of a property | GET    | http://localhost:8000/api/properties/<<int:id>>/ |
| Update a property         | PUT    | http://localhost:8000/api/properties/<<int:id>>/ |
| Delete a property         | DELETE | http://localhost:8000/api/properties/<<int:id>>/ |

### Request/Response DATA:

JSON Body to send data:

-   Create (POST) a property:

```
{
  "street": "string",
  "city": "string",
  "state": "string",
  "zipcode": 0,
  "beds": 0,
  "baths": 0,
  "property_manager": 0,
  "tenant": 0,
  "community": 0
}
```

-   Update (PUT) a property by <<int:id>>:

```
{
  "beds": 0,
  "baths": 0,
  "property_manager": 0,
  "tenant": 0,
  "community": 0
}
```

JSON Body returned data:

-   List (GET) properties or (GET/PUT) property detail by <<int:id>> (property.id):

```
{
	"properties": [
      {
        "id": 0,
        "street": "string",
        "city": "string",
        "state": "string",
        "zipcode": 0,
        "beds": 0,
        "baths": 0,
        "property_manager": 0,
        "tenant": 0,
        "community": 0,
        "created_on": "2024-06-24T00:41:12.028Z"
    },
  ]
}
```

-   Delete a Listing item by <<int:id>>: (NOTE: "id" will be null on success)

```
{
	"deleted": "Deleted Property"
}
```

### MAINTENANCE Table and Endpoints:

| ACTION                        | METHOD | URL                                               |
| ----------------------------- | ------ | ------------------------------------------------- |
| List maintenance              | GET    | http://localhost:8000/api/                        |
| Create a maintenance          | POST   | http://localhost:8000/api/maintenance/<<int:id>>/ |
| Update a specific maintenance | PUT    | http://localhost:8000/maintenance/<<int:id>>/     |
| Delete a specific maintenance | DELETE | http://localhost:8000/api/maintenance/<<int:id>>/ |

### Request/Response DATA:

JSON Body to send data:

-   Create (POST) a maintenance item: \*tenant

```
{
  "issue": "string",
  "attempts": "string",
  "outcome": "string",
  "assigned_personnel": "string",
  "status": 0,
  "maker": 0,
  "property_id": 0
}
```

-   Update (PUT) a maintenance item:

```
{
  "issue": "string",
  "attempts": "string",
  "outcome": "string",
  "assigned_personnel": "string",
  "status": 0,
  "resolved": true,
  "resolved_on": "2024-06-24"
}
```

JSON Body returned data:

-   List (GET) maintenance items by property.id or (GET/PUT) maintenance item detail by <<int:id>> (maintenance.id):

```
{
	"maintenance": [
        {
          "id": 0,
          "issue": "string",
          "attempts": "string",
          "outcome": "string",
          "assigned_personnel": "string",
          "status": "string",
          "resolved": true,
          "resolved_on": "2024-06-24",
          "maker": 0,
          "first_name": "string",
          "last_name": "string",
          "property_manager": 0,
          "property_id": 0,
          "street": "string",
          "city": "string",
          "state": "string",
          "zipcode": 0,
          "created_on": "2024-06-24T00:44:00.688Z"
      },
  ]
}
```

-   Delete a maintenance item by <<int:id>>: (NOTE: "id" will be null on success)

```
{
	"deleted": "Deleted Maintenance Request"
}
```

### BILLS Table and Endpoints

| ACTION                 | METHOD | URL                                         |
| ---------------------- | ------ | ------------------------------------------- |
| List bills             | GET    | http://localhost:8000/api/bills/            |
| POST bill              | POST   | http://localhost:8000/api/bills/            |
| Get bill detail        | GET    | http://localhost:8000/api/bills/<<int:id>>/ |
| Update bill            | PUT    | http://localhost:8000/api/bills/<<int:id>>/ |
| Delete a specific bill | DELETE | http://localhost:8000/bills/<<int:id>>/     |

### Request/Response DATA

JSON Body to send data:

-   Create (POST) a Bill by <<int:id:>>:

```
{
  "tenant": 0,
  "property_manager": 0,
  "link": "string",
  "bill_name": "string",
  "bill_type": "string",
  "due_date": "2024-06-24",
  "amount": 0,
  "complete": true,
  "property_id": 0
}
```

-   Update (PUT) by <<int:id:>>:

```
{
  "tenant": 0,
  "property_manager": 0,
  "link": "string",
  "bill_name": "string",
  "bill_type": "string",
  "due_date": "2024-06-24",
  "amount": 0,
  "complete": true,
  "property_id": 0
}
```

JSON Body returned data:

-   List (GET) Bills (tenant.id) or (GET/PUT) Bill detail by <<int:id>> (bill.id):
    -Tenant and Property_Manager View:

```
{
  "bills": [
      {
        "id": 0,
        "tenant": 0,
        "property_manager": 0,
        "link": "string",
        "bill_name": "string",
        "bill_type": "string",
        "due_date": "2024-06-24",
        "amount": 0,
        "complete": true,
        "property_id": 0,
        "created_on": "2024-06-24T00:47:36.150Z"
    },
  ]
}
```

-   Delete a bill by <<int:id>>:

```
{
	"deleted": "Deleted Bill."
}
```

### Review Table and Endpoints:

| ACTION          | METHOD | URL                                           |
| --------------- | ------ | --------------------------------------------- |
| List reviews    | GET    | http://localhost:8000/api/reviews/            |
| Create a review | POST   | http://localhost:8000/reviews/                |
| Review detail   | GET    | http://localhost:8000/api/reviews/<<int:id>>/ |
| Update a review | PUT    | http://localhost:8000/api/reviews/<<int:id>>/ |
| Delete a review | DELETE | http://localhost:8000/api/reviews/<<int:id>>/ |

### Request/Response DATA

JSON Body to send data:

-   Create (POST) a review:

```
{
  "title": "string",
  "experience": "string",
  "reviewer": 0,
  "rating": 0,
  "property_id": 0
}
```

-   Update (PUT) a review by <<int:id>> (user.id - the receiving user):

```
{
  "title": "string",
  "experience": "string",
  "rating": 0
}
```

JSON Body returned Data:

-   List (GET) reviews or (GET/PUT) review detail by <<int:id>>:

```
{
  "reviews": [
      {
        "id": 0,
        "title": "string",
        "experience": "string",
        "reviewer": 0,
        "reviewer_name": "string",
        "rating": 0,
        "property_id": 0,
        "created_on": "2024-06-24T00:52:38.384Z"
    },
  ]
}
```

-   Delete a review by <<int:id>>:

```
{
	"deleted": "Deleted review."
}
```
