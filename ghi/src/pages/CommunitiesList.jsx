import useAuthService from '../hooks/useAuthService'
import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { baseUrl } from '../services/authService'

export default function CommunitiesList() {
    const navigate = useNavigate()
    const { user, isLoggedIn } = useAuthService()
    const [specifiedUser, setSpecifiedUser] = useState([])
    const [communities, setCommunities] = useState([])

    const communitiesData = async () => {
        const url = `${baseUrl}/api/communities/`
        try {
            const response = await fetch(url, {
                method: 'get',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application.json',
                },
            })
            if (response.ok) {
                const communitiesList = await response.json()
                console.log(communitiesList)
                setCommunities(communitiesList)
            }
        } catch (error) {
            console.error('Failed to query url.', error)
        }
    }

    const userData = async () => {
        try {
            const user_id = user.id
            const url = `${baseUrl}/api/users/${user_id}`
            const fetchConfig = {
                method: "delete",
                headers: {
                    "Content-Type": "application/json",
                },
                credentials: "include"
            }
            const response = await fetch(url, fetchConfig)
            if (response.ok) {
                const usersData = await response.json()
                setSpecifiedUser(usersData)
            } else {
                console.error('Failed to fetch user data.')
            }
        } catch (error) {
            console.log('Error fetching user.', error)
        }
    }

    useEffect(() => {
        communitiesData(), userData()
    }, [])

    if (isLoggedIn && specifiedUser.role === 1) {
        return (
            <div>
                <div>
                    <button
                        type="button"
                        onClick={() => navigate('/communities/create')}
                    >
                        Create a New Community
                    </button>
                </div>
                <h1>List of Communities</h1>
                <table>
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Creator: First</th>
                            <th>& Last Name</th>
                            <th>Description</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Zipcode</th>
                            <th>Rating</th>
                            <th>Created On</th>
                        </tr>
                    </thead>
                    <tbody>
                        {communities.map((community) => {
                            return (
                                <tr key={community.id}>
                                    <td>{community.title}</td>
                                    <td>{community.first_name}</td>
                                    <td>{community.last_name}</td>
                                    <td>{community.description}</td>
                                    <td>{community.city}</td>
                                    <td>{community.state}</td>
                                    <td>{community.zipcode}</td>
                                    <td>{community.rating}</td>
                                    <td>{community.created_on}</td>
                                    <td>
                                        <button
                                            type="button"
                                            onClick={() =>
                                                navigate(
                                                    '/communities/' +
                                                        community.id
                                                )
                                            }
                                        >
                                            Update Community
                                        </button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
    if (isLoggedIn) {
        return (
            <div>
                <h1>List of Communities</h1>
                <table>
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Creator: First</th>
                            <th>& Last Name</th>
                            <th>Description</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Zipcode</th>
                            <th>Rating</th>
                            <th>Created On</th>
                        </tr>
                    </thead>
                    <tbody>
                        {communities.map((community) => {
                            return (
                                <tr key={community.id}>
                                    <td>{community.title}</td>
                                    <td>{community.first_name}</td>
                                    <td>{community.last_name}</td>
                                    <td>{community.description}</td>
                                    <td>{community.city}</td>
                                    <td>{community.state}</td>
                                    <td>{community.zipcode}</td>
                                    <td>{community.rating}</td>
                                    <td>{community.created_on}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
    return (
        <div>
            <h1>List of Communities</h1>
            <table>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Rating</th>
                        <th>Created On</th>
                    </tr>
                </thead>
                <tbody>
                    {communities.map((community) => {
                        return (
                            <tr key={community.id}>
                                <td>{community.title}</td>
                                <td>{community.description}</td>
                                <td>{community.city}</td>
                                <td>{community.state}</td>
                                <td>{community.rating}</td>
                                <td>{community.created_on}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
