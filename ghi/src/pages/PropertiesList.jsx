import useAuthService from '../hooks/useAuthService'
import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { baseUrl } from '../services/authService'

export default function PropertiesList() {
    const { isLoggedIn } = useAuthService()
    const [properties, setProperties] = useState([])

    const propertiesData = async () => {
        const url = `${baseUrl}/api/properties/`
        try {
            const response = await fetch(url, {
                method: 'get',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application.json',
                },
            })
            if (response.ok) {
                const propertiesList = await response.json()
                setProperties(propertiesList)
            }
        } catch (error) {
            console.error('Failed to query url.', error)
        }
    }

    useEffect(() => {
        propertiesData()
    }, [])

    if (isLoggedIn) {
        return (
            <div>
                <h1>List of Properties</h1>
                <table>
                    <thead>
                        <tr>
                            <th>Street</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Zipcode</th>
                            <th>Beds</th>
                            <th>Baths</th>
                            <th>Property_manager</th>
                            <th>Tenant</th>
                            <th>Community</th>
                            <th>Listed Since</th>
                        </tr>
                    </thead>
                    <tbody>
                        {properties.map((property) => {
                            return (
                                <tr key={property.id}>
                                    <td>{property.street}</td>
                                    <td>{property.city}</td>
                                    <td>{property.state}</td>
                                    <td>{property.zipcode}</td>
                                    <td>{property.beds}</td>
                                    <td>{property.baths}</td>
                                    <td>{property.property_manager}</td>
                                    <td>{property.tenant}</td>
                                    <td>{property.community}</td>
                                    <td>{property.created_on}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
    return (
        <div>
            <h1>List of Properties</h1>
            <table>
                <thead>
                    <tr>
                        <th>Street</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Zipcode</th>
                        <th>Beds</th>
                        <th>Baths</th>
                        <th>Community</th>
                    </tr>
                </thead>
                <tbody>
                    {properties.map((property) => {
                        return (
                            <tr key={property.id}>
                                <td>{property.street}</td>
                                <td>{property.city}</td>
                                <td>{property.state}</td>
                                <td>{property.zipcode}</td>
                                <td>{property.beds}</td>
                                <td>{property.baths}</td>
                                <td>{property.community}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
