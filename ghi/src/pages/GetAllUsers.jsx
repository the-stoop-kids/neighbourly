import { useEffect, useState } from 'react'
import { baseUrl } from '../services/authService'
import useAuthService from '../hooks/useAuthService'

export default function UsersList() {
    const { isLoggedIn } = useAuthService()
    const [usersData, setUsersData] = useState([])

    const fetchUsersData = async () => {
        try {
            const url = `${baseUrl}/api/users/`
            const res = await fetch(url)
            if (res.ok) {
                const data = await res.json()
                setUsersData(data)
            } else {
                console.error('Failed to fetch user data.', res.statusText)
            }
        } catch (error) {
            console.error('Failed to query url.', error)
        }
    }

    useEffect(() => {
        fetchUsersData()
    }, [])

    if (isLoggedIn) {
        return (
            <div>
                <h1>Users</h1>
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>E-mail</th>
                            <th>Phone Number</th>
                            <th>Role</th>
                        </tr>
                    </thead>
                    <tbody>
                        {usersData.map((user) => {
                            return (
                                <tr key={user.id}>
                                    <th>{user.id}</th>
                                    <td>
                                        {user.first_name} {user.last_name}
                                    </td>
                                    <td>{user.email}</td>
                                    <td>{user.phone_number}</td>
                                    {user.role === 1 ? (
                                        <>
                                            <td>Property Manager</td>
                                        </>
                                    ) : (
                                        <>
                                            <td>Tenant</td>
                                        </>
                                    )}
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
    return (
        <div>
            <h1>Please log in to view this page.</h1>
        </div>
    )
}
