import { useEffect, useState } from 'react'
import { baseUrl } from '../services/authService'
import useAuthService from '../hooks/useAuthService'
import { useNavigate } from 'react-router-dom'

export default function BillsList() {
    const { user, isLoggedIn } = useAuthService()
    const [billsData, setBillsData] = useState([])
    const today = new Date().toISOString().slice(0, 10)
    const [usersData, setUsersData] = useState([])
    const [deleted, setDeleted] = useState(false)
    const [isOpen, setIsOpen] = useState(false)
    const navigate = useNavigate()

    const fetchBillsData = async () => {
        try {
            const url = `${baseUrl}/api/bills/`
            const fetchConfig = {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json',
                },
                credentials: 'include',
            }
            const res = await fetch(url, fetchConfig)
            if (res.ok) {
                const data = await res.json()
                setBillsData(data)
            } else {
                console.error('Failed to fetch bill data.', res.statusText)
            }
        } catch (error) {
            console.error('Failed to query url.', error)
        }
    }

    const fetchUsersData = async () => {
        try {
            const user_id = user.id
            const url = `${baseUrl}/api/users/${user_id}`
            const fetchConfig = {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json',
                },
                credentials: 'include',
            }
            const res = await fetch(url, fetchConfig)
            if (res.ok) {
                const data = await res.json()
                setUsersData(data)
            } else {
                console.error('Falied to fetch user data.', res.statusText)
            }
        } catch (error) {
            console.error('Failed to query url.', error)
        }
    }

    useEffect(() => {
        fetchBillsData(), fetchUsersData()
    }, [])

    const handleDeleteClick = async (bill) => {
        const bill_id = bill.id
        const url = `${baseUrl}/api/bills/${bill_id}`
        const fetchConfig = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json',
            },
            credentials: 'include',
        }

        try {
            const response = await fetch(url, fetchConfig)
            if (response.ok) {
                setDeleted(true)
                const newBillsData = billsData.filter((b) => b.id != bill_id)
                setBillsData(newBillsData)
            } else {
                console.log('There was an issue deleting this bill.')
            }
        } catch (error) {
            console.error('Failed to submit delete request.', error)
        }
    }

    if (usersData.role === 1 && isLoggedIn) {
        return (
            <div>
                {deleted && (
                    <div>
                        Bill deleted successfully!
                    </div>
                )}
                <h1>Bills</h1>
                    <p>Completed</p>
                    <table>
                        <thead>
                            <tr>
                                <th>Bill Name</th>
                                <th>Bill Type</th>
                                <th>Link</th>
                                <th>Due Date</th>
                                <th>Amount</th>
                                <th>
                                    Update
                                </th>
                                <th>
                                    Delete
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        {billsData.filter((bill) => today < bill.due_date && bill.complete === false && bill.property_manager === user.id)
                        .map((bill) => {
                            return (
                                <tr key={bill.id}>
                                    <td>{bill.bill_name}</td>
                                    <td>{bill.bill_type}</td>
                                    <td>{bill.link}</td>
                                    <td>{bill.due_date}</td>
                                    <td>{bill.amount}</td>
                                    <td>
                                        <button
                                            type="button"
                                            onClick={() => navigate("/bills/" + bill.id)}
                                        >
                                            Update?
                                        </button>
                                    </td>
                                    <td>
                                        {isOpen ? (
                                            <>
                                                <button
                                                    type="button"
                                                    onClick={() => setIsOpen(false)}
                                                >
                                                    Cancel
                                                </button>
                                                <button
                                                    type="button"
                                                    onClick={() => {
                                                        handleDeleteClick(bill),
                                                        setIsOpen(false)
                                                    }}
                                                >
                                                    Confirm
                                                </button>
                                            </>
                                        ) : null }
                                        {!isOpen ? (
                                            <button type="button" onClick={() => setIsOpen(true)}>
                                                Delete?
                                            </button>
                                        ) : null }
                                    </td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>

                    <p>Overdue</p>
                    <table>
                        <thead>
                            <tr>
                                <th>Bill Name</th>
                                <th>Bill Type</th>
                                <th>Link</th>
                                <th>Due Date</th>
                                <th>Amount</th>
                                <th>
                                    Update
                                </th>
                                <th>
                                    Delete
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        {billsData.filter((bill) => today > bill.due_date && bill.complete === false && bill.property_manager === user.id)
                        .map((bill) => {
                            return (
                                <tr key={bill.id}>
                                    <td>{bill.bill_name}</td>
                                    <td>{bill.bill_type}</td>
                                    <td>{bill.link}</td>
                                    <td>{bill.due_date}</td>
                                    <td>{bill.amount}</td>
                                    <td>
                                        <button
                                            type="button"
                                            onClick={() => navigate("/bills/" + bill.id)}
                                        >
                                            Update?
                                        </button>
                                    </td>
                                    <td>
                                        {isOpen ? (
                                            <>
                                                <button
                                                    type="button"
                                                    onClick={() => setIsOpen(false)}
                                                >
                                                    Cancel
                                                </button>
                                                <button
                                                    type="button"
                                                    onClick={() => {
                                                        handleDeleteClick(bill),
                                                        setIsOpen(false)
                                                    }}
                                                >
                                                    Confirm
                                                </button>
                                            </>
                                        ) : null }
                                        {!isOpen ? (
                                            <button type="button" onClick={() => setIsOpen(true)}>
                                                Delete?
                                            </button>
                                        ) : null }
                                    </td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>

                    <p>Paid</p>
                    <table>
                        <thead>
                            <tr>
                                <th>Bill Name</th>
                                <th>Bill Type</th>
                                <th>Link</th>
                                <th>Due Date</th>
                                <th>Amount</th>
                                <th>
                                    Update
                                </th>
                                <th>
                                    Delete
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        {billsData.filter((bill) => bill.complete === true && bill.property_manager === user.id)
                        .map((bill) => {
                            return (
                                <tr key={bill.id}>
                                    <td>{bill.bill_name}</td>
                                    <td>{bill.bill_type}</td>
                                    <td>{bill.link}</td>
                                    <td>{bill.due_date}</td>
                                    <td>{bill.amount}</td>
                                    <td>
                                        <button
                                            type="button"
                                            onClick={() => navigate("/bills/" + bill.id)}
                                        >
                                            Update?
                                        </button>
                                    </td>
                                    <td>
                                        {isOpen ? (
                                            <>
                                                <button
                                                    type="button"
                                                    onClick={() => setIsOpen(false)}
                                                >
                                                    Cancel
                                                </button>
                                                <button
                                                    type="button"
                                                    onClick={() => {
                                                        handleDeleteClick(bill),
                                                        setIsOpen(false)
                                                    }}
                                                >
                                                    Confirm
                                                </button>
                                            </>
                                        ) : null }
                                        {!isOpen && (
                                            <button type="button" onClick={() => setIsOpen(true)}>
                                                Delete?
                                            </button>
                                        )}
                                    </td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>
                <div>
                    <div>
                        <div>
                            <button
                                type="button"
                                onClick={() => navigate("/bills/create")}
                            >
                                Add a Bill
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    } else if (usersData.role === 2 && isLoggedIn) {
        // need to add logic for only tenant bills here
        return (
            <div>
                <h1>Bills</h1>
                    <p>Due</p>
                    <table>
                        <thead>
                            <tr>
                                <th>Bill Name</th>
                                <th>Bill Type</th>
                                <th>Link</th>
                                <th>Due Date</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                        {billsData.filter((bill) => today < bill.due_date && bill.complete === false && bill.tenant === user.id)
                        .map((bill) => {
                            return (
                                <tr key={bill.id}>
                                    <td>{bill.bill_name}</td>
                                    <td>{bill.bill_type}</td>
                                    <td>{bill.link}</td>
                                    <td>{bill.due_date}</td>
                                    <td>{bill.amount}</td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>

                    <p>Outstanding</p>
                    <table>
                        <thead>
                            <tr>
                                <th>Bill Name</th>
                                <th>Bill Type</th>
                                <th>Link</th>
                                <th>Due Date</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                        {billsData.filter((bill) => today > bill.due_date && bill.complete === false && bill.tenant === user.id)
                        .map((bill) => {
                            return (
                                <tr key={bill.id}>
                                    <td>{bill.bill_name}</td>
                                    <td>{bill.bill_type}</td>
                                    <td>{bill.link}</td>
                                    <td>{bill.due_date}</td>
                                    <td>{bill.amount}</td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>

                    <p>Completed</p>
                    <table>
                        <thead>
                            <tr>
                                <th>Bill Name</th>
                                <th>Bill Type</th>
                                <th>Link</th>
                                <th>Due Date</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                        {billsData.filter((bill) => bill.complete === true && bill.tenant === user.id)
                        .map((bill) => {
                            return (
                                <tr key={bill.id}>
                                    <td>{bill.bill_name}</td>
                                    <td>{bill.bill_type}</td>
                                    <td>{bill.link}</td>
                                    <td>{bill.due_date}</td>
                                    <td>{bill.amount}</td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>
            </div>
        )
    } else {
        return <div>Please log in to view this page.</div>
    }
}
