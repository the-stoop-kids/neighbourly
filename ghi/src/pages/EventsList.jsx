import useAuthService from '../hooks/useAuthService'
import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { baseUrl } from '../services/authService'

export default function EventsList() {
    const navigate = useNavigate()
    const { user, isLoggedIn } = useAuthService()
    const [events, setEvents] = useState([])

    const eventsData = async () => {
        const response = await fetch(`${baseUrl}/api/events/`, {
            method: 'get',
            credentials: 'include',
            headers: {
                'Content-Type': 'application.json',
            },
        })
        if (response.ok) {
            const eventsList = await response.json()
            setEvents(eventsList)
        }
    }

    useEffect(() => {
        eventsData()
    }, [])

    const handleDeleteEvent = async (event) => {
        const event_id = event.id
        const deleteEvent = await fetch(`${baseUrl}/api/events/${event_id}/`, {
            method: 'delete',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
        })
        try {
            if (deleteEvent.ok) {
                const deleted = events.filter((event) => event.id !== event_id)
                setEvents(deleted)
            } else {
                console.log('There was an error deleting this event.')
            }
        } catch (error) {
            console.log('Failed to complete request to delete event.', error)
        }
    }

    if (isLoggedIn) {
        return (
            <div>
                <div>
                    <button
                        type="button"
                        onClick={() => navigate('/events/create')}
                    >
                        Create a New Event
                    </button>
                </div>
                <h1>List of Events</h1>
                <table>
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Maker</th>
                            <th>Location</th>
                            <th>Description</th>
                            <th>Event Date</th>
                            <th>Zipcode</th>
                            <th>Category</th>
                            <th>Created On</th>
                        </tr>
                    </thead>
                    <tbody>
                        {events.map((event) => {
                            return (
                                <tr key={event.id}>
                                    <td>{event.title}</td>
                                    <td>{event.maker}</td>
                                    <td>{event.location}</td>
                                    <td>{event.description}</td>
                                    <td>{event.event_date}</td>
                                    <td>{event.zipcode}</td>
                                    <td>{event.category}</td>
                                    <td>{event.created_on}</td>
                                    {user.id == event.maker ? (
                                        <>
                                            <td>
                                                <button
                                                    type="button"
                                                    className="e-link"
                                                    onClick={() =>
                                                        navigate(
                                                            '/events/' +
                                                                event.id
                                                        )
                                                    }
                                                >
                                                    Update Event
                                                </button>
                                            </td>
                                            <td>
                                                <button
                                                    type="button"
                                                    className="e-link"
                                                    onClick={() =>
                                                        handleDeleteEvent(event)
                                                    }
                                                >
                                                    Delete Event
                                                </button>
                                            </td>
                                        </>
                                    ) : null}
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
    return (
        <div>
            <h1>List of Events</h1>
            <table>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Maker</th>
                        <th>Location</th>
                        <th>Description</th>
                        <th>Event Date</th>
                        <th>Zipcode</th>
                        <th>Category</th>
                        <th>Created On</th>
                    </tr>
                </thead>
                <tbody>
                    {events.map((event) => {
                        return (
                            <tr key={event.id}>
                                <td>{event.title}</td>
                                <td>{event.maker}</td>
                                <td>{event.location}</td>
                                <td>{event.description}</td>
                                <td>{event.event_date}</td>
                                <td>{event.zipcode}</td>
                                <td>{event.category}</td>
                                <td>{event.created_on}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
