import { useEffect, useState } from "react";
import { baseUrl } from "../services/authService";
import useAuthService from "../hooks/useAuthService";
import { useNavigate } from "react-router-dom";
import { styled } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import Stack from '@mui/material/Stack';
import { Link } from 'react-router-dom';
import Container from "@mui/material/Container";
import Snackbar from '@mui/material/Snackbar';


const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#b3e5fc',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));


export default function ReviewsList() {

    const { user, isLoggedIn } = useAuthService();
    const [ reviewsData, setReviewsData] = useState([]);
    const [ deleted, setDeleted ] = useState(false);
    const [ isOpen, setIsOpen ] = useState(false);
    const navigate = useNavigate();

    const fetchReviewsData = async () => {
        try {
            const url = `${baseUrl}/api/reviews/`
            const res = await fetch(url)
            if (res.ok) {
                const data = await res.json()
                setReviewsData(data)
            } else {
                console.error('Failed to fetch reviews data.', res.statusText)
            }
        } catch (error) {
            console.error('Failed to query url.', error)
        }
    }

    useEffect(() => {
        fetchReviewsData()
    }, [])

    const handleDeleteClick = async (review) => {

        const review_id = review.id
        const url = `${baseUrl}/api/reviews/${review_id}`
        const fetchConfig = {
            method: "delete",
            headers: {
                "Content-Type": "application/json",
            },
            credentials: "include"
        }

        try {
            const response = await fetch (url, fetchConfig)
            if (response.ok) {
                setDeleted(true)
                const newReviewsData = reviewsData.filter((r) => r.id != review_id)
                setReviewsData(newReviewsData)
            } else {
                console.log('There was an issue deleting this bill.')
            }
        } catch (error) {
            console.error('Failed to submit delete request.', error)
        }
    }

    function formatDate() {
		const date = new Date().toISOString().slice(0,10);
		return date;
	}

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
        return;
        }

        setDeleted(false);
    };

    if (isLoggedIn) {
        return (
            <Container
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center'
                }}
            >
                <Typography variant='h2'>Reviews</Typography>
                <Box sx={{
                    width: '100%',
                    display: "flex",
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
                >
                    <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
                    {reviewsData.map((review) => (
                        <Grid item xs={6} key={review.id}>
                            <Item>
                                <Typography variant='h3'>{review.title}</Typography>
                                <Typography variant='h7'>Author: {review.reviewer_name}</Typography>
                                <Typography variant='h5'>{review.experience}</Typography>
                                {review.property_id ? (
                                    <Typography variant='body2'>Property: {review.property_id}</Typography>
                                ) : null}
                                {!review.property_id ?(
                                    <Typography variant='body2'>No Property associated</Typography>
                                ) : null }
                                <Typography variant='body2'>Rating: {review.rating}</Typography>
                                <Typography variant='body2'>{formatDate(review.created_on)}</Typography>
                                {review.reviewer === user.id && isOpen ? (
                                    <>
                                        <Stack direction="row" spacing={2} justifyContent='center'>
                                            <Button
                                                variant='contained'
                                                onClick={() => navigate("/reviews/" + review.id)}
                                            >
                                                Update
                                            </Button>
                                            <Button
                                                variant='contained'
                                                onClick={() => setIsOpen(false)}
                                            >
                                                Cancel
                                            </Button>
                                            <Button
                                                variant='contained'
                                                onClick={() => {
                                                    handleDeleteClick(review),
                                                    setIsOpen(false)
                                                }}
                                            >
                                                Confirm
                                            </Button>
                                            <Snackbar
                                                open={deleted}
                                                autoHideDuration={5000}
                                                onClose={handleClose}
                                                message="Review deleted."
                                            />
                                        </Stack>
                                    </>
                                ) : null}
                                {review.reviewer === user.id && !isOpen ? (
                                    <>
                                        <Stack direction="row" spacing={2} justifyContent='center' >
                                            <Button
                                                variant='contained'
                                                onClick={() => navigate("/reviews/" + review.id)}
                                            >
                                                Update
                                            </Button>
                                            <Button
                                                variant='contained'
                                                startIcon={<DeleteIcon />}
                                                onClick={() => setIsOpen(true)}
                                            >
                                                Delete
                                            </Button>
                                        </Stack>
                                    </>
                                ) : null}
                            </Item>
                        </Grid>
                    ))}
                    </Grid>
                    <Button
                        component={Link}
                        variant='contained'
                        sx={{ marginTop: '7px' }}
                        to="/reviews/create"
                        size='large'
                    >
                        Add a Review?
                    </Button>
                </Box>
            </Container>
        )
    } else {
        return (
            <Container
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center'
                }}
            >
                <Typography variant='h2'>Reviews</Typography>
                <Box sx={{
                    width: '100%',
                    display: "flex",
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
                >
                    <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
                    {reviewsData.map((review) => (
                        <Grid item xs={6} key={review.id}>
                            <Item>
                                <Typography variant='h3'>{review.title}</Typography>
                                <Typography variant='h7'>Author: {review.reviewer_name}</Typography>
                                <Typography variant='h5'>{review.experience}</Typography>
                                {review.property_id ? (
                                    <Typography variant='body2'>Property: {review.property_id}</Typography>
                                ) : null}
                                {!review.property_id ?(
                                    <Typography variant='body2'>No Property associated</Typography>
                                ) : null }
                                <Typography variant='body2'>Rating: {review.rating}</Typography>
                                <Typography variant='body2'>{formatDate(review.created_on)}</Typography>
                            </Item>
                        </Grid>
                    ))}
                    </Grid>
                </Box>
            </Container>
        )
    }
}
