import React from 'react'
import useAuthService from '../hooks/useAuthService'
import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { baseUrl } from '../services/authService'

export default function MaintenanceList() {
    const navigate = useNavigate()
    const { user, isLoggedIn } = useAuthService()
    const [userData, setUserData] = useState()
    const [specificProperty, setSpecificProperty] = useState([])
    const [maintenance, setMaintenance] = useState([])

    const user_id = user?.id

    useEffect(() => {
        const specificUserData = async () => {
            const res = await fetch(`${baseUrl}/api/users/${user_id}/`, {
                method: 'get',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json',
                },
            })
            let userInfo
            if (res.ok) {
                const userResponse = await res.json()
                setUserData(userResponse)
                userInfo = userResponse
            } else {
                console.log('Failed to fetch user.')
            }
            const response = await fetch(`${baseUrl}/api/properties/`)
            if (response.ok) {
                const propertiesData = await response.json()
                if (userInfo?.role === 2) {
                    for (let property of propertiesData) {
                        if (property.tenant === userInfo?.id) {
                            setSpecificProperty(property)
                        } else {
                            console.error(
                                'You are not a tenant at any property.'
                            )
                        }
                    }
                } else {
                    for (let property of propertiesData) {
                        const propertiesArray = []
                        if (property.property_manager === userInfo?.id) {
                            propertiesArray.push(property)
                        }
                        setSpecificProperty(propertiesArray)
                    }
                }
            } else {
                console.error('Failed to fetch user data.')
            }
            const url = `${baseUrl}/api/maintenance/`
            try {
                const response = await fetch(url, {
                    method: 'get',
                    credentials: 'include',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                })
                if (response.ok) {
                    const maintenanceList = await response.json()
                    setMaintenance(maintenanceList)
                }
            } catch (error) {
                console.error('Failed to query url.', error)
            }
        }
        if (isLoggedIn) {
            specificUserData()
        }
    }, [])

    return isLoggedIn ? (
        specificProperty.tenant === user?.id ? (
            <div>
                <div>
                    <button
                        type="button"
                        onClick={() => navigate('/maintenance/create')}
                    >
                        Create a New Maintenance
                    </button>
                </div>
                <h1>List of Maintenance</h1>
                <table>
                    <thead>
                        <tr>
                            <th>Maker</th>
                            <th>Issue</th>
                            <th>Attempts</th>
                            <th>Outcome</th>
                            <th>Property</th>
                            <th>Assigned Personnel</th>
                            <th>Status</th>
                            <th>Resolved</th>
                            <th>Resolved On</th>
                            <th>Created On</th>
                        </tr>
                    </thead>
                    <tbody>
                        {maintenance
                            .filter((item) => item.maker === userData.id)
                            .map((item) => {
                                return (
                                    <tr key={item.id}>
                                        <td>
                                            {item.first_name}
                                            {item.last_name}
                                        </td>
                                        <td>{item.issue}</td>
                                        <td>{item.attempts}</td>
                                        <td>{item.outcome}</td>
                                        <td>
                                            {item.street}
                                            {item.city}
                                            {item.state}
                                            {item.zipcode}
                                        </td>
                                        <td>{item.assigned_personnel}</td>
                                        <td>{item.status}</td>
                                        <td>{item.resolved}</td>
                                        <td>{item.resolved_on}</td>
                                        <td>{item.created_on}</td>
                                        <td>
                                            <button
                                                type="button"
                                                className="e-link"
                                                onClick={() =>
                                                    navigate(
                                                        '/maintenance/' +
                                                            item.id
                                                    )
                                                }
                                            >
                                                Update Maintenance
                                            </button>
                                        </td>
                                    </tr>
                                )
                            })}
                    </tbody>
                </table>
            </div>
        ) : specificProperty.length > 0 ? (
            <div>
                <h1>List of Maintenance</h1>
                <table>
                    <thead>
                        <tr>
                            <th>Maker</th>
                            <th>Issue</th>
                            <th>Attempts</th>
                            <th>Outcome</th>
                            <th>Property</th>
                            <th>Assigned Personnel</th>
                            <th>Status</th>
                            <th>Resolved</th>
                            <th>Resolved On</th>
                            <th>Created On</th>
                        </tr>
                    </thead>
                    <tbody>
                        {maintenance
                            .filter(
                                (item) => item.property_manager === userData.id
                            )
                            .map((item) => {
                                return (
                                    <tr key={item.id}>
                                        <td>
                                            {item.first_name}
                                            {item.last_name}
                                        </td>
                                        <td>{item.issue}</td>
                                        <td>{item.attempts}</td>
                                        <td>{item.outcome}</td>
                                        <td>
                                            {item.street}
                                            {item.city}
                                            {item.state}
                                            {item.zipcode}
                                        </td>
                                        <td>{item.assigned_personnel}</td>
                                        <td>{item.status}</td>
                                        <td>{item.resolved}</td>
                                        <td>{item.resolved_on}</td>
                                        <td>{item.created_on}</td>
                                        <td>
                                            <button
                                                type="button"
                                                className="e-link"
                                                onClick={() =>
                                                    navigate(
                                                        '/maintenance/' +
                                                            item.id
                                                    )
                                                }
                                            >
                                                Update Maintenance
                                            </button>
                                        </td>
                                    </tr>
                                )
                            })}
                    </tbody>
                </table>
            </div>
        ) : (
            <div>
                Maintenance tickets will show here when user is associated with
                a property.
            </div>
        )
    ) : (
        <div>You need to be logged in to view this page.</div>
    )
}
