import { useEffect, useState } from "react";
import { baseUrl } from "../services/authService";
import useAuthService from "../hooks/useAuthService";
import { useNavigate } from "react-router-dom";


export default function UserPage() {

    const { user, isLoggedIn } = useAuthService()
    const [ profileData, setProfileData ] = useState([])
    const navigate = useNavigate()

    const fetchUsersData = async () => {
        try {
            const user_id = user.id
            const url =  `${baseUrl}/api/users/${user_id}`
            const res = await fetch(url)
            if (res.ok) {
                const data = await res.json()
                setProfileData([data])
            } else {
                console.error('Failed to fetch user data.', res.statusText)
            }
        } catch (error) {
            console.error('Failed to query url.', error)
        }
    }

    useEffect(() => {
        fetchUsersData()
    }, [])

    if (isLoggedIn) {
        return (
            <>
            <div>
                <div>
                {profileData.map((profile) => {
                    return (
                        <>
                        <div>
                            <div>
                                <div>
                                    <div>
                                        <img
                                            src={profile.picture_url}
                                            alt={profile.username}
                                        />
                                        <div>
                                            <h4>{profile.username}</h4>
                                            <p>
                                                {profile.role === 1 ? <>Property Manager</> : <>Tenant</>}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div>
                                <div>
                                    <div>
                                        <div>
                                            <h6>Full Name</h6>
                                        </div>
                                        <div>
                                            {profile.first_name} {profile.last_name}
                                        </div>
                                    </div>
                                    <hr />
                                    <div>
                                        <div>
                                            <h6>Email</h6>
                                        </div>
                                        <div>
                                            {profile.email}
                                        </div>
                                    </div>
                                    <hr />
                                    <div>
                                        <div>
                                            <h6>Phone</h6>
                                        </div>
                                        <div>
                                            {profile.phone_number}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </>
                    )
                })}
                </div>
            </div>
            <div>
                <button
                    type="button"
                    onClick={() => navigate("/useredit")}
                >
                    Update account?
                </button>
                <button
                    type="button"
                    onClick={() => navigate("/userpasswordreset")}
                >
                    Reset Password?
                </button>
            </div>
            </>
        )
    } else {
        return (
            <div>Please log in to view account page.</div>
        )
    }

}
