import Container from '@mui/material/Container'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'


const HomeForm = () => {

    return (
        <Container>
            <Box
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center"
                }}
            >
                <Typography
                    variant='h2'
                    align='center'
                >
                    Unlock Your Potential
                </Typography>
                <Typography>
                    Neighbourly Rental Manager opens new horizons for your
                    business with a robust, all-in-one platform. Whatever
                    your needs, our tailored partnership and user-friendly
                    software removes obstacles, helping you discover new
                    efficiencies and drive business growth.
                </Typography>
            </Box>
        </Container>
    )
}

export default HomeForm
