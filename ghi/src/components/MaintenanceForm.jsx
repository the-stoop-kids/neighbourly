import useAuthService from '../hooks/useAuthService'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { baseUrl } from '../services/authService'

export default function MaintenanceForm() {
    const navigate = useNavigate()
    const { user, isLoggedIn } = useAuthService()
    const [property, setProperty] = useState([])
    const [maintenanceFormData, setMaintenanceFormData] = useState({
        issue: '',
        attempts: '',
        outcome: '',
        assigned_personnel: '',
        status: '',
        maker: '',
    })

    const propertyData = async () => {
        const user_id = user.id
        const response = await fetch(`${baseUrl}/api/properties/`)
        if (response.ok) {
            const propertiesData = await response.json()
            for (let property of propertiesData) {
                if (property.tenant === user_id) {
                    setProperty(property)
                } else if (property.property_manager === user_id) {
                    setProperty(property)
                } else {
                    console.error(
                        'You are not a user associated with this property'
                    )
                }
            }
        } else {
            console.error('Failed to fetch user data.')
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name
        setMaintenanceFormData((prevState) => ({
            ...prevState,
            [inputName]: value,
        }))
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            const createMaintenance = async () => {
                const response = await fetch(`${baseUrl}/api/maintenance/`, {
                    method: 'post',
                    credentials: 'include',
                    body: JSON.stringify({
                        ...maintenanceFormData,
                        status: 1,
                        maker: user?.id,
                        property_id: property.id,
                    }),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                })

                if (response.ok) {
                    setMaintenanceFormData({
                        issue: '',
                        attempts: '',
                        outcome: '',
                        assigned_personnel: '',
                        status: '',
                        maker: '',
                    })
                    navigate('/maintenance')
                }
            }
            await createMaintenance(maintenanceFormData)
        } catch (error) {
            console.error(error)
            throw error
        }
    }

    useEffect(() => {
        propertyData()
    }, [])

    if (isLoggedIn && user.id === property.tenant) {
        return (
            <div>
                <h1>Create a Maintenance Ticket</h1>
                <form onSubmit={handleSubmit}>
                    <div>
                        <input
                            value={maintenanceFormData.issue}
                            onChange={handleFormChange}
                            placeholder="Issue"
                            id="issue"
                            required
                            type="text"
                            name="issue"
                            className="form-control"
                        />
                    </div>
                    <div>
                        <input
                            value={maintenanceFormData.attempts}
                            onChange={handleFormChange}
                            placeholder="Attempts"
                            required
                            id="attempts"
                            type="text"
                            name="attempts"
                            className="form-control"
                        />
                    </div>
                    <div>
                        <input
                            value={maintenanceFormData.outcome}
                            onChange={handleFormChange}
                            placeholder="Outcome"
                            required
                            id="outcome"
                            type="text"
                            name="outcome"
                            className="form-control"
                        />
                    </div>
                    <button type="submit" className="btn btn-primary">
                        Create
                    </button>
                </form>
            </div>
        )
    }
    return (
        <div>
            <p>
                You do not have the permissions to create a maintenance ticket
                at this time. Please login as a tenant.
            </p>
        </div>
    )
}
