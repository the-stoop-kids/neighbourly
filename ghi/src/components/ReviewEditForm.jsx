import { useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import useAuthService from '../hooks/useAuthService'
import { baseUrl } from "../services/authService";


export default function EditReviewform() {

    const { user, isLoggedIn } = useAuthService();
    const { review_id } = useParams();
    const navigate = useNavigate();
    const [ formData, setFormData ] = useState({
        "id": "",
        "title": "",
        "experience": "",
        "reviewer": user?.id,
        "rating": "",
        "property_id": "",
        "created_on": "",
    });

    const fetchReviewData = async () => {
        try {
            const url = `${baseUrl}/api/reviews/${review_id}`
            const res = await fetch(url)
            if (res.ok) {
                const data = await res.json()
                setFormData(data)
            } else {
                console.error('Failed to fetch reviews data.', res.statusText)
            }
        } catch (error) {
            console.error('Failed to query url.', error)
        }
    }

    useEffect(() => {
        fetchReviewData()
    }, [review_id])

    const handleFormChange = (e) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    const handleSubmission = async (e) => {
        e.preventDefault();

        setFormData((prevState) => ({
            ...prevState,
        }));

        const url = `${baseUrl}/api/reviews/${review_id}`
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
            },
            credentials: "include",
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            console.log('Review was updated successfully!')
            navigate("/reviews")
        } else {
            console.log('There was an error updating the review.')
        }
    }

    if (isLoggedIn && formData.reviewer === user.id) {
        return (
            <div>
                <form
                    onSubmit={handleSubmission}
                    className='mb-3'
                >
                    <div className='row mb-3'>
                        <label>Title</label>
                        <input
                            type="text"
                            required
                            name="title"
                            id="title"
                            value={formData.title}
                            onChange={handleFormChange}
                        />
                    </div>
                    <div className='row mb-3'>
                        <label>Experience</label>
                        <input
                            type="text"
                            required
                            name="experience"
                            id="experience"
                            value={formData.experience}
                            onChange={handleFormChange}
                        />
                    </div>
                    <div className='row mb-3'>
                        <label>Rating</label>
                        <select
                            onChange={handleFormChange}
                            name="rating"
                            id="rating"
                            className="form-select"
                            value={formData.rating}
                        >
                            <option value="">Select one</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <div>
                        <button type="submit" className='btn btn-primary'>Save Changes?</button>
                    </div>
                </form>
            </div>
        )
    } else if (isLoggedIn) {
        return (
            <div>You do not have permission to update this review.</div>
        )
    }
    return (
        <div>Please log in to update this review.</div>
    )

}
