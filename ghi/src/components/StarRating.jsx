import { useState } from 'react'
import { FaStar } from 'react-icons/fa'


function StarRating(props) {
    const [hover, setHover] = useState(null)

    return (
        <>
            <div>
                {[...Array(5)].map((_, index) => {
                    const currentRating = index + 1
                    return (
                        <label key={currentRating}>
                            <input
                                type="radio"
                                name="rating"
                                value={currentRating}
                                onClick={() => props.setRating(currentRating)}
                            />
                            <FaStar
                                className="star"
                                size={20}
                                value={props.rating}
                                color={
                                    currentRating <= (hover || props.rating)
                                        ? '#ffc107'
                                        : '#e4e5e9'
                                }
                                onMouseEnter={() => setHover(currentRating)}
                                onMouseLeave={() => setHover(null)}
                            />
                        </label>
                    )
                })}
                <p> Your rating is {props.rating}</p>
            </div>
        </>
    )
}

export default StarRating
