import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import useAuthService from '../hooks/useAuthService'
import { baseUrl } from "../services/authService";


export default function EditUserForm() {

    const { user, isLoggedIn } = useAuthService()
    const navigate = useNavigate()
    const [ userFormData, setUserFormData ] = useState({
        "id": "",
        "username": "",
        "first_name": "",
        "last_name": "",
        "picture_url": "",
        "email": "",
        "phone_number": "",
        "role": "",
        "created_on": "",
    })

    const fetchUserData = async () => {
        try {
            const user_id = user.id
            const url =  `${baseUrl}/api/users/${user_id}`
            const fetchConfig = {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json',
                },
                credentials: 'include',
            }
            const res = await fetch(url, fetchConfig)
            if (res.ok) {
                const data = await res.json()
                setUserFormData(data)
            } else {
                console.error('Failed to fetch user data.', res.statusText)
            }
        } catch (error) {
            console.error('Failed to query url.', error)
        }
    }

    useEffect(() => {
        fetchUserData()
    }, [])

    const handleFormChange = (e) => {
        const { name, value } = e.target;
        setUserFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    const handleSubmission = async (e) => {
        e.preventDefault();

        setUserFormData((prevState) => ({
            ...prevState,
        }));

        const user_id = user.id
        const url = `${baseUrl}/api/users/${user_id}`
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(userFormData),
            headers: {
                "Content-Type": "application/json",
            },
            credentials: "include",
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            console.log('User was updated successfully!')
            navigate("/userprofile")
        } else {
            console.log('There was an error updating the user.')
        }
    }

    if (isLoggedIn) {
        return (
            <div>
                <form
                    onSubmit={handleSubmission}
                >
                    <div className='row mb-3'>
                        <input
                            type="text"
                            required
                            name="username"
                            id="username"
                            value={userFormData.username}
                            onChange={handleFormChange}
                        />
                    </div>
                    <div>
                        <div>
                            <input
                                type="text"
                                required
                                name="first_name"
                                id="first_name"
                                value={userFormData.first_name}
                                onChange={handleFormChange}
                            />
                        </div>
                        <div className='col'>
                            <input
                                type="text"
                                required
                                name="last_name"
                                id="last_name"
                                value={userFormData.last_name}
                                onChange={handleFormChange}
                            />
                        </div>
                    </div>
                    <div className='row mb-3'>
                        <input
                            type="text"
                            name="picture_url"
                            id="picture_url"
                            value={userFormData.picture_url}
                            onChange={handleFormChange}
                        />
                    </div>
                    <div className='row mb-3'>
                        <input
                            type="text"
                            required
                            name="email"
                            id="email"
                            value={userFormData.email}
                            onChange={handleFormChange}
                        />
                    </div>
                    <div className='row mb-3'>
                        <input
                            type="text"
                            required
                            name="phone_number"
                            id="phone_number"
                            value={userFormData.phone_number}
                            onChange={handleFormChange}
                        />
                    </div>
                    <div className='row mb-3'>
                        <select
                            onChange={handleFormChange}
                            name="role"
                            id="role"
                            value={userFormData.role}
                        >
                            <option value="">Select a Role</option>
                            <option value="1">Property Manager</option>
                            <option value="2">Tenant</option>
                        </select>
                    </div>
                    <div>
                        <button type="submit">Update</button>
                    </div>
                </form>
            </div>
        )
    } else {
        return (
            <div>Please log in to update User data.</div>
        )
    }
}
