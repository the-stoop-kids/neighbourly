import { useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import useAuthService from '../hooks/useAuthService'
import { baseUrl } from "../services/authService";


export default function EditBillform() {
    const { user, isLoggedIn } = useAuthService()
    const [ usersData, setUsersData ] = useState([])
    const { bill_id } = useParams()
    const navigate = useNavigate()
    const [ billForm, setBillForm ] = useState({
        "id":"",
        "tenant": "",
        "property_manager": "",
        "link": "",
        "bill_name": "",
        "bill_type": "",
        "due_date": "",
        "amount": "",
        "complete": "",
        "property_id": "",
        "created_on": "",
    })

    const getBillData = async () => {
        try {
            const url = `${baseUrl}/api/bills/${bill_id}`
            const fetchConfig = {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json',
                },
                credentials: 'include',
            }
            const response = await fetch(url, fetchConfig)
            const billData = await response.json()
            setBillForm(billData)
        } catch (error) {
            console.error('Failed to fetch bill.', error)
        }
    }

    const fetchUsersData = async () => {
        try {
            const user_id = user.id
            const fetchConfig = {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json',
                },
                credentials: 'include',
            }
            const url =  `${baseUrl}/api/users/${user_id}`
            const res = await fetch(url, fetchConfig)
            if (res.ok) {
                const data = await res.json()
                setUsersData(data)
            } else {
                console.error('Falied to fetch user data.', res.statusText)
            }
        } catch (error) {
            console.error('Failed to query url.', error)
        }
    }

    useEffect(() => {
        getBillData(),
        fetchUsersData()
    }, [bill_id])

    const handleFormChange = (e) => {
        const { name, value } = e.target;
        setBillForm((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    const handleSubmission = async (e) => {
        e.preventDefault();

        setBillForm((prevState) => ({
            ...prevState,
        }));

        const url = `${baseUrl}/api/bills/${bill_id}`
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(billForm),
            headers: {
                "Content-Type": "application/json",
            },
            credentials: "include",
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            console.log('Bill was updated successfully!')
            navigate("/bills")
        } else {
            console.log('There was an error updating the bill.')
        }
    }

    if (isLoggedIn && usersData.role === 1) {
        return (
            <form
                    onSubmit={handleSubmission}
                >
                    <div>
                        <label>Bill Link</label>
                        <input
                            type="text"
                            required
                            name="link"
                            id="link"
                            value={billForm.link}
                            onChange={handleFormChange}
                        />
                    </div>
                    <div>
                        <label>Bill Name</label>
                        <input
                            type="text"
                            required
                            name="bill_name"
                            id="bill_name"
                            value={billForm.bill_name}
                            onChange={handleFormChange}
                        />
                    </div>
                    <div>
                        <label>Bill Type</label>
                        <input
                            type="text"
                            name="bill_type"
                            id="bill_type"
                            value={billForm.bill_type}
                            onChange={handleFormChange}
                        />
                    </div>
                    <div>
                        <label>Bill Due Date</label>
                        <input
                            type="text"
                            required
                            name="due_date"
                            id="due_date"
                            value={billForm.due_date}
                            onChange={handleFormChange}
                        />
                    </div>
                    <div>
                        <label>Bill Amount</label>
                        <input
                            type="text"
                            required
                            name="amount"
                            id="amount"
                            value={billForm.amount}
                            onChange={handleFormChange}
                        />
                    </div>
                    <div>
                        <label>Completed?</label>
                        <select
                            onChange={handleFormChange}
                            name="complete"
                            id="complete"
                            value={billForm.complete}
                        >
                            <option value="">Select one</option>
                            <option value="false">Due</option>
                            <option value="true">Paid</option>
                        </select>
                    </div>
                    <div>
                        <button type="submit">Save Changes?</button>
                    </div>
                </form>
        )
    } else if (isLoggedIn && usersData.role !== 1) {
        return (
            <div>You do not have the required permissions to view this page.</div>
        )
    }
    return (
        <div>Please log in to view this page.</div>
    )
}
