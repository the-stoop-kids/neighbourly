import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import useAuthService from '../hooks/useAuthService'
import { baseUrl } from '../services/authService'

export default function ReviewForm() {
    const { user, isLoggedIn } = useAuthService()
    const [propertyData, setPropertyData] = useState([])
    const navigate = useNavigate()
    const [reviewFormData, setReviewFormData] = useState({
        title: '',
        experience: '',
        reviewer: user?.id,
        rating: '',
        property_id: '0',
    })

    const fetchPropertyData = async () => {
        try {
            const url = `${baseUrl}/api/properties/`
            const res = await fetch(url)
            if (res.ok) {
                const data = await res.json()
                setPropertyData(data)
            } else {
                console.error('Failed to fetch property data.', res.statusText)
            }
        } catch (error) {
            console.error('Failed to query property url.', error)
        }
    }

    const handleFormChange = (e) => {
        const { name, value } = e.target
        setReviewFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }))
    }

    const handleSubmission = async (e) => {
        e.preventDefault()

        setReviewFormData((prevState) => ({
            ...prevState,
        }))

        const createNewReview = async () => {
            const url = `${baseUrl}/api/reviews/`
            const fetchConfig = {
                method: 'post',
                body: JSON.stringify(reviewFormData),
                headers: {
                    'Content-Type': 'application/json',
                },
                credentials: 'include',
            }
            const res = await fetch(url, fetchConfig)
            if (res.ok) {
                console.log('Review created successfully.')
                navigate('/reviews')
            } else {
                console.log('Failed to create a review.')
            }
        }

        await createNewReview(reviewFormData)
    }

    useEffect(() => {
        fetchPropertyData()
    })

    if (isLoggedIn) {
        return (
            <form onSubmit={handleSubmission} className="mb-3">
                <div className="row mb-3">
                    <input
                        type="text"
                        required
                        name="title"
                        id="title"
                        value={reviewFormData.title}
                        onChange={handleFormChange}
                        placeholder="Enter a title..."
                    />
                </div>
                <div className="row mb-3">
                    <input
                        type="text"
                        required
                        name="experience"
                        id="experience"
                        value={reviewFormData.experience}
                        onChange={handleFormChange}
                        placeholder="Describe your experience..."
                    />
                </div>
                <div>
                    <select
                        onChange={handleFormChange}
                        name="rating"
                        id="rating"
                        className="form-select"
                        value={reviewFormData.rating}
                    >
                        <option value="">Select a rating...</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
                <div>
                    <select
                        onChange={handleFormChange}
                        name="property_id"
                        id="property_id"
                        className="form-select"
                        value={reviewFormData.property_id}
                    >
                        <option value="0">Select a property (optional)</option>
                        {propertyData
                            .filter(
                                (property) =>
                                    property.property_manager === user?.id
                            )
                            .map((property) => {
                                return (
                                    <option
                                        key={property.id}
                                        value={property.id}
                                    >
                                        {property.id}: {property.street},{' '}
                                        {property.city}, {property.state},{' '}
                                        {property.zipcode}
                                    </option>
                                )
                            })}
                    </select>
                </div>
                <div>
                    <button type="submit" className="btn btn-primary">
                        Create Review
                    </button>
                </div>
            </form>
        )
    } else {
        ;<div>Please log in to create a review.</div>
    }
}
