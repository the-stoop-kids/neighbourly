import { useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import useAuthService from '../hooks/useAuthService'
import { baseUrl } from '../services/authService'
import StarRating from './StarRating'

export default function CommunityEdit() {
    const navigate = useNavigate()
    const [rating, setRating] = useState(null)
    const { user, isLoggedIn } = useAuthService()
    const [specifiedUser, setSpecifiedUser] = useState([])
    const { communityId } = useParams()
    const [community, setCommunity] = useState({
        title: '',
        owner: user?.id,
        description: '',
        city: '',
        state: '',
        zipcode: '',
    })

    const communityData = async () => {
        try {
            const communityUrl = `${baseUrl}/api/communities/${communityId}/`
            const response = await fetch(communityUrl)
            if (response.ok) {
                const data = await response.json()
                if (data.owner == user.id) {
                    setCommunity(data)
                } else {
                    console.error(
                        'You do not have permission to edit this community.'
                    )
                }
            } else {
                console.error('Error fetching community.')
            }
        } catch (error) {
            console.error('Error fetching community:', error)
        }
    }
    const userData = async () => {
        try {
            const user_id = user.id
            const response = await fetch(`${baseUrl}/api/users/${user_id}`)
            if (response.ok) {
                const usersData = await response.json()
                setSpecifiedUser(usersData)
            } else {
                console.error('Failed to fetch user data.')
            }
        } catch (error) {
            console.log('Error fetching user.', error)
        }
    }

    useEffect(() => {
        communityData(), userData()
    }, [communityId])

    const handleChange = (e) => {
        const { name, value } = e.target
        setCommunity((prevState) => ({
            ...prevState,
            [name]: value,
        }))
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        setCommunity((prevState) => ({
            ...prevState,
        }))
        try {
            const response = await fetch(
                `${baseUrl}/api/communities/${communityId}/`,
                {
                    method: 'put',
                    credentials: 'include',
                    body: JSON.stringify({
                        ...community,
                        rating: rating,
                    }),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }
            )
            if (response.ok) {
                navigate('/communities')
            } else {
                console.log('There was an error updating the community.')
            }
        } catch (error) {
            console.error('Error occurred.', error)
        }
    }

    if (isLoggedIn && specifiedUser.role === 1) {
        return (
            <form onSubmit={handleSubmit}>
                <div>
                    <label>Community Title</label>
                    <input
                        type="text"
                        required
                        name="title"
                        id="title"
                        value={community.title}
                        onChange={handleChange}
                    />
                </div>
                <div>
                    <label>Description</label>
                    <input
                        type="text"
                        required
                        name="description"
                        id="description"
                        value={community.description}
                        onChange={handleChange}
                    />
                </div>
                <div>
                    <label>City</label>
                    <input
                        type="text"
                        name="city"
                        id="city"
                        value={community.city}
                        onChange={handleChange}
                    />
                </div>
                <div>
                    <label>State</label>
                    <input
                        type="text"
                        required
                        name="state"
                        id="state"
                        value={community.state}
                        onChange={handleChange}
                    />
                </div>
                <div>
                    <label>Zipcode</label>
                    <input
                        type="text"
                        required
                        name="zipcode"
                        id="zipcode"
                        value={community.zipcode}
                        onChange={handleChange}
                    />
                </div>
                <div>
                    <>
                        <StarRating rating={rating} setRating={setRating} />
                    </>
                </div>
                <div>
                    <button type="submit">
                        Save Changes
                    </button>
                </div>
            </form>
        )
    } else if (isLoggedIn && specifiedUser.role !== 1) {
        return (
            <div>
                <p>You do not have the permissions to view this page.</p>
            </div>
        )
    }
    return (
        <div>
            <p>
                Please log into your property manager portal to view this page.
            </p>
        </div>
    )
}
