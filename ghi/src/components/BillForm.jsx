import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import useAuthService from '../hooks/useAuthService'
import { baseUrl } from "../services/authService";


export default function NewBillForm() {

    const { user, isLoggedIn } = useAuthService()
    const [ tenantData, setTenantData ] = useState([])
    const [ propertyData, setPropertyData ] = useState([])
    const [ usersData, setUsersData ] = useState([])
    const navigate = useNavigate()
    const [ formData, setFormData ] = useState({
        "tenant": "",
        "property_manager": user?.id,
        "link": "",
        "bill_name": "",
        "bill_type": "",
        "due_date": "",
        "amount": "",
        "complete": "false",
        "property_id": "",
    });

    const fetchTenantData = async () => {
        try {
            const fetchConfig = {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json',
                },
                credentials: 'include',
            }
            const url =  `${baseUrl}/api/users/`
            const res = await fetch(url, fetchConfig)
            if (res.ok) {
                const data = await res.json()
                setTenantData(data)
            } else {
                console.error('Failed to fetch user data.', res.statusText)
            }
        } catch (error) {
            console.error('Failed to query user url.', error)
        }
    }

    const fetchPropertyData = async () => {
        try {
            const url = `${baseUrl}/api/properties/`
            const fetchConfig = {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json',
                },
                credentials: 'include',
            }
            const res = await fetch(url, fetchConfig)
            if (res.ok) {
                const data = await res.json()
                setPropertyData(data)
            } else {
                console.error('Failed to fetch property data.', res.statusText)
            }
        } catch (error) {
            console.error('Failed to query property url.', error)
        }
    }

    const fetchUsersData = async () => {
        try {
            const user_id = user.id
            const fetchConfig = {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json',
                },
                credentials: 'include',
            }
            const url =  `${baseUrl}/api/users/${user_id}`
            const res = await fetch(url, fetchConfig)
            if (res.ok) {
                const data = await res.json()
                setUsersData(data)
            } else {
                console.error('Falied to fetch user data.', res.statusText)
            }
        } catch (error) {
            console.error('Failed to query url.', error)
        }
    }

    const handleFormChange = (e) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    const handleSubmission = async (e) => {
        e.preventDefault();

        setFormData((prevState) => ({
            ...prevState,
        }));

        const createNewBill = async () => {
            const url = `${baseUrl}/api/bills/`
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(formData),
                headers: {
                    "Content-Type": "application/json",
                },
                credentials: "include",
            };
            const res = await fetch(url, fetchConfig)
            if (res.ok) {
                console.log('Bill created successfully.')
                navigate("/bills")
            } else {
                console.log('Failed to create a bill.')
            }
        }

        await createNewBill(formData)
    };

    useEffect(() => {
        fetchTenantData(),
        fetchPropertyData(),
        fetchUsersData()
    }, [])

    if (isLoggedIn && usersData.role === 1) {
        return (
            <form
                onSubmit={handleSubmission}
            >
                <div className='row mb-3'>
                    <select
                        onChange={handleFormChange}
                        name="tenant"
                        id="tenant"
                        value={formData.tenant}
                    >
                        <option value="">Select a Tenant</option>
                        {tenantData.filter((tenant) => tenant.role === 2)
                        .map((tenant) => {
                            return (
                                <option value={tenant.id}>{tenant.first_name} {tenant.last_name}</option>
                            )
                        })}
                    </select>
                </div>
                <div>
                    <input
                        type="text"
                        required
                        name="link"
                        id="link"
                        value={formData.link}
                        onChange={handleFormChange}
                        placeholder="Enter a link to the bill"
                    />
                </div>
                <div>
                    <input
                        type="text"
                        required
                        name="bill_name"
                        id="bill_name"
                        value={formData.bill_name}
                        onChange={handleFormChange}
                        placeholder="Enter the name of the bill"
                    />
                </div>
                <div>
                    <input
                        type="text"
                        name="bill_type"
                        id="bill_type"
                        value={formData.bill_type}
                        onChange={handleFormChange}
                        placeholder="Enter a bill type"
                    />
                </div>
                <div>
                    <input
                        type="text"
                        required
                        name="due_date"
                        id="due_date"
                        value={formData.due_date}
                        onChange={handleFormChange}
                        placeholder="Enter a due date: YYYY-MM-DD"
                    />
                </div>
                <div>
                    <input
                        type="text"
                        required
                        name="amount"
                        id="amount"
                        value={formData.amount}
                        onChange={handleFormChange}
                        placeholder="Enter bill amount"
                    />
                </div>
                <div>
                    <select
                        onChange={handleFormChange}
                        name="complete"
                        id="complete"
                        value={formData.complete}
                    >
                        <option value="">Select one</option>
                        <option value="false">Due</option>
                        <option value="true">Paid</option>
                    </select>
                </div>
                <div>
                    <select
                        onChange={handleFormChange}
                        name="property_id"
                        id="property_id"
                        value={formData.property_id}
                    >
                        <option value="">Select a property</option>
                        {propertyData.filter((property) => property.property_manager === user?.id)
                        .map((property) => {
                            return (
                                <option value={property.id}>
                                    {property.id}: {property.street}, {property.city}, {property.state}, {property.zipcode}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <div>
                    <button type="submit">Create Bill</button>
                </div>
            </form>
        )
    } else if (isLoggedIn && usersData.role !== 1) {
        return (
            <div>You do not have the required permissions to view this page.</div>
        )
    }
    return (
        <div>Please log in to view this page.</div>
    )
}
