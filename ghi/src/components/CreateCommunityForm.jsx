import useAuthService from '../hooks/useAuthService'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { baseUrl } from '../services/authService'
import StarRating from './StarRating'

export default function CreateCommunity() {
    const navigate = useNavigate()
    const { user, isLoggedIn } = useAuthService()
    const [specifiedUser, setSpecifiedUser] = useState([])
    const [rating, setRating] = useState(null)
    const [communityFormData, setCommunityFormData] = useState({
        title: '',
        description: '',
        city: '',
        state: '',
        zipcode: '',
    })

    const userData = async () => {
        try {
            const user_id = user.id
            const response = await fetch(`${baseUrl}/api/users/${user_id}`)
            if (response.ok) {
                const usersData = await response.json()
                setSpecifiedUser(usersData)
            } else {
                console.error('Failed to fetch user data.')
            }
        } catch (error) {
            console.log('Error fetching user.', error)
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name
        setCommunityFormData((prevState) => ({
            ...prevState,
            [inputName]: value,
        }))
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            const createCommunity = async () => {
                const response = await fetch(`${baseUrl}/api/communities/`, {
                    method: 'post',
                    credentials: 'include',
                    body: JSON.stringify({
                        ...communityFormData,
                        rating: rating,
                        owner: user?.id,
                    }),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                })

                if (response.ok) {
                    setCommunityFormData({
                        title: '',
                        owner: '',
                        description: '',
                        city: '',
                        state: '',
                        zipcode: '',
                        rating: '',
                    })
                    navigate('/communities')
                }
            }
            await createCommunity(communityFormData)
        } catch (error) {
            console.error(error)
            throw error
        }
    }

    useEffect(() => {
        userData()
    }, [])

    if (isLoggedIn && specifiedUser.role === 1) {
        return (
            <div>
                <h1>Create a Community</h1>
                <form onSubmit={handleSubmit}>
                    <div>
                        <input
                            value={communityFormData.title}
                            onChange={handleFormChange}
                            placeholder="Title"
                            id="title"
                            required
                            type="text"
                            name="title"
                        />
                    </div>
                    <div>
                        <input
                            value={communityFormData.description}
                            onChange={handleFormChange}
                            placeholder="Description"
                            required
                            id="description"
                            type="text"
                            name="description"
                        />
                    </div>
                    <div>
                        <input
                            value={communityFormData.city}
                            onChange={handleFormChange}
                            placeholder="City"
                            required
                            id="city"
                            type="text"
                            name="city"
                        />
                    </div>
                    <div>
                        <input
                            value={communityFormData.state}
                            onChange={handleFormChange}
                            placeholder="State"
                            required
                            id="state"
                            type="text"
                            name="state"
                        />
                    </div>
                    <div>
                        <input
                            value={communityFormData.zipcode}
                            onChange={handleFormChange}
                            placeholder="Zipcode"
                            required
                            id="zipcode"
                            type="text"
                            name="zipcode"
                        />
                    </div>
                    <div>
                        <>
                            <StarRating rating={rating} setRating={setRating} />
                        </>
                    </div>
                    <button
                        type="submit"
                    >
                        Create
                    </button>
                </form>
            </div>
        )
    } else if (isLoggedIn && specifiedUser.role !== 1) {
        return (
            <div>
                <p>You do not have the permissions to view this page.</p>
            </div>
        )
    }
    return (
        <div>
            <p>
                Please log into your property manager portal to view this page.
            </p>
        </div>
    )
}
