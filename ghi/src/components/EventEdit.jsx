import { useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import useAuthService from '../hooks/useAuthService'
import { baseUrl } from '../services/authService'

export default function EventEdit() {
    const navigate = useNavigate()
    const { user, isLoggedIn } = useAuthService()
    const { event_id } = useParams()
    const [event, setEvent] = useState({
        title: '',
        description: '',
        location: '',
        event_date: '',
        maker: user?.id,
        category: '',
    })

    const eventData = async () => {
        try {
            const response = await fetch(`${baseUrl}/api/events/${event_id}`)
            if (response.ok) {
                const data = await response.json()
                setEvent(data)
            } else {
                console.error('Failed to fetch events.', response.statusText)
            }
        } catch (error) {
            console.error('Failed to query that url.', error)
        }
    }

    useEffect(() => {
        eventData()
    }, [event_id])

    const handleChange = (e) => {
        const { name, value } = e.target
        setEvent((prevState) => ({
            ...prevState,
            [name]: value,
        }))
    }

    const handleSubmit = async (e) => {
        e.preventDefault()

        setEvent((prevState) => ({
            ...prevState,
        }))
        try {
            const response = await fetch(`${baseUrl}/api/events/${event_id}/`, {
                method: 'put',
                credentials: 'include',
                body: JSON.stringify(event),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
            if (response.ok) {
                navigate('/events')
            } else {
                console.error('There was an error updating this event.')
            }
        } catch (error) {
            console.error('Error occurred.', error)
        }
    }

    if (isLoggedIn && event.maker === user.id) {
        return (
            <div>
                <form onSubmit={handleSubmit}>
                    <div>
                        <label>Location</label>
                        <input
                            type="text"
                            required
                            name="location"
                            id="location"
                            value={event.location}
                            onChange={handleChange}
                        />
                    </div>
                    <div>
                        <label>Event Date</label>
                        <input
                            type="date"
                            required
                            name="event_date"
                            id="event_date"
                            value={event.event_date}
                            onChange={handleChange}
                        />
                    </div>
                    <div>
                        <button type="submit" className="btn btn-primary">
                            Save Changes
                        </button>
                    </div>
                </form>
            </div>
        )
    }
    return <div>You do not have permission to update this event.</div>
}
