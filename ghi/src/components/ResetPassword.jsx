import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import useAuthService from '../hooks/useAuthService'
import { baseUrl } from "../services/authService";


export default function ResetPassword() {

    const { user, isLoggedIn } = useAuthService()
    const navigate = useNavigate()
    const [ userFormData, setUserFormData ] = useState({
        "username": "",
        "password": "",
        "password2": "",
    })

    const handleFormChange = (e) => {
        const { name, value } = e.target;
        setUserFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    const handleSubmission = async (e) => {
        e.preventDefault();

        setUserFormData((prevState) => ({
            ...prevState,
        }));

        if (userFormData.password === userFormData.password2) {
            const user_id = user.id
            const url = `${baseUrl}/api/users/${user_id}/reset-password/`
            const fetchConfig = {
                method: "put",
                body: JSON.stringify(userFormData),
                headers: {
                    "Content-Type": "application/json",
                },
                credentials: "include",
            }
            const response = await fetch(url, fetchConfig)
            if (response.ok) {
                console.log('Password was updated successfully!')
                navigate("/userprofile")
            } else {
                console.log('There was an error updating the password.')
            }
        } else {
            console.error('Passwords did not match. Please try again.')
        }
    }

    if (isLoggedIn) {
        return (
            <div>
                <form
                    onSubmit={handleSubmission}
                >
                    <div>
                        <input
                            type="text"
                            required
                            name="username"
                            id="username"
                            value={userFormData.username}
                            onChange={handleFormChange}
                            placeholder='Please enter your username'
                        />
                    </div>
                    <div>
                        <input
                            type="text"
                            required
                            name="password"
                            id="password"
                            value={userFormData.password}
                            onChange={handleFormChange}
                            placeholder='Please enter a new password'
                        />
                    </div>
                    <div>
                        <input
                            type="text"
                            required
                            name="password2"
                            id="password2"
                            value={userFormData.password2}
                            onChange={handleFormChange}
                            placeholder='Please confirm your new password'
                        />
                    </div>
                    <div>
                        <button type="submit">Reset Password?</button>
                    </div>
                </form>
            </div>
        )
    } else {
        return (
            <div>Please log in to reset your password.</div>
        )
    }
}
