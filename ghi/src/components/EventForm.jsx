import useAuthService from '../hooks/useAuthService'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { baseUrl } from '../services/authService'

export default function EventForm() {
    const navigate = useNavigate()
    const { user, isLoggedIn } = useAuthService()
    const [eventData, setEventData] = useState({
        title: '',
        description: '',
        location: '',
        event_date: '',
        maker: user?.id,
        category: '',
    })

    const handleChange = (e) => {
        const { name, value } = e.target
        setEventData((prevState) => ({
            ...prevState,
            [name]: value,
        }))
    }

    const handleSubmit = async (e) => {
        e.preventDefault()

        setEventData((prevState) => ({
            ...prevState,
        }))

        const createEvent = async () => {
            const response = await fetch(`${baseUrl}/api/events/`, {
                method: 'post',
                credentials: 'include',
                body: JSON.stringify({
                    ...eventData,
                    maker: user?.id,
                }),
                headers: {
                    'Content-Type': 'application/json',
                },
            })

            if (response.ok) {
                navigate('/events')
            } else {
                console.log('Failed to create an event.')
            }
        }
        await createEvent(eventData)
    }

    if (isLoggedIn) {
        return (
            <div>
                <h1>Create an Event</h1>
                <form onSubmit={handleSubmit}>
                    <div>
                        <input
                            value={eventData.title}
                            onChange={handleChange}
                            placeholder="Title"
                            id="title"
                            required
                            type="text"
                            name="title"
                            className="form-control"
                        />
                    </div>
                    <div>
                        <input
                            value={eventData.description}
                            onChange={handleChange}
                            placeholder="Description"
                            required
                            id="description"
                            type="text"
                            name="description"
                            className="form-control"
                        />
                    </div>
                    <div>
                        <input
                            value={eventData.location}
                            onChange={handleChange}
                            placeholder="Location"
                            required
                            id="location"
                            type="text"
                            name="location"
                            className="form-control"
                        />
                    </div>
                    <div>
                        <input
                            value={eventData.event_date}
                            onChange={handleChange}
                            placeholder="Event Date"
                            required
                            id="event_date"
                            type="date"
                            name="event_date"
                            className="form-control"
                        />
                    </div>
                    <div>
                        <select
                            onChange={handleChange}
                            name="category"
                            id="category"
                            className="form-select"
                            value={eventData.category}
                        >
                            <option value="">Select a category...</option>
                            <option value="1">Activity/Recreation</option>
                            <option value="2">Announcement</option>
                            <option value="3">Buy/Sell</option>
                            <option value="4">Charity Event</option>
                            <option value="5">Conference</option>
                            <option value="6">Entertainment</option>
                            <option value="7">Exhibit</option>
                            <option value="8">Food/Drink</option>
                            <option value="9">Local Gathering</option>
                            <option value="10">Meeting</option>
                            <option value="11">Networking</option>
                            <option value="12">Performance</option>
                            <option value="13">Workshop/Seminar</option>
                            <option value="14">Virtual Event</option>
                            <option value="15">
                                Other (Please specify in title and description)
                            </option>
                        </select>
                    </div>
                    <button type="submit" className="btn btn-primary">
                        Create
                    </button>
                </form>
            </div>
        )
    }
    return (
        <div>
            <p>Please log into your account to create an event.</p>
        </div>
    )
}
