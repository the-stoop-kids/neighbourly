import { useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import useAuthService from '../hooks/useAuthService'
import { baseUrl } from '../services/authService'

export default function MaintenanceEdit() {
    const navigate = useNavigate()
    const { maintenance_id } = useParams()
    const { user, isLoggedIn } = useAuthService()
    const [maintenanceForm, setMaintenanceForm] = useState({
        issue: '',
        attempts: '',
        outcome: '',
        assigned_personnel: '',
        status: '',
        resolved: '',
        resolved_on: '',
        created_on: '',
        property_id: '',
        property_manager: '',
        tenant_name: '',
    })

    const maintenanceData = async () => {
        const maintenanceUrl = `${baseUrl}/api/maintenance/${maintenance_id}`
        const response = await fetch(maintenanceUrl)
        if (response.ok) {
            const data = await response.json()
            setMaintenanceForm(data)
        } else {
            console.error('Error fetching that maintenance ticket.')
        }
    }

    useEffect(() => {
        maintenanceData()
    }, [maintenance_id])

    const handleChange = async (e) => {
        const { name, value } = e.target
        setMaintenanceForm((prevMaintenanceForm) => ({
            ...prevMaintenanceForm,
            [name]: value,
        }))
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        setMaintenanceForm((prevState) => ({
            ...prevState,
        }))
        try {
            const response = await fetch(
                `${baseUrl}/api/maintenance/${maintenance_id}/`,
                {
                    method: 'put',
                    credentials: 'include',
                    body: JSON.stringify(maintenanceForm),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }
            )

            if (response.ok) {
                navigate('/maintenance')
            } else {
                console.error(
                    'There was an error updating this maintenance ticket.'
                )
            }
        } catch (error) {
            console.error('Error occurred.', error)
        }
    }

    if (isLoggedIn && user?.id === maintenanceForm.maker) {
        return (
            <div>
                <h1>Update Maintenance Ticket</h1>
                <form onSubmit={handleSubmit}>
                    <div>
                        <label>Maintenance Issue</label>
                        <input
                            type="text"
                            required
                            name="issue"
                            id="issue"
                            value={maintenanceForm.issue}
                            onChange={handleChange}
                        />
                    </div>
                    <div>
                        <label>Attempts</label>
                        <input
                            type="text"
                            required
                            name="attempts"
                            id="attempts"
                            value={maintenanceForm.attempts}
                            onChange={handleChange}
                        />
                    </div>

                    <div>
                        <label>Outcome</label>
                        <input
                            type="text"
                            name="outcome"
                            id="outcome"
                            value={maintenanceForm.outcome}
                            onChange={handleChange}
                        />
                    </div>
                    <div>
                        <button type="submit" className="btn btn-primary">
                            Save Changes
                        </button>
                    </div>
                </form>
            </div>
        )
    }
    if (isLoggedIn && user?.id === maintenanceForm.property_manager) {
        return (
            <div>
                <h1>Update Maintenance Ticket</h1>
                <form onSubmit={handleSubmit}>
                    <div>
                        <label>Assigned Personnel</label>
                        <input
                            type="text"
                            name="assigned_personnel"
                            id="assigned_personnel"
                            value={maintenanceForm.assigned_personnel}
                            onChange={handleChange}
                        />
                    </div>
                    <div>
                        <label>Status</label>
                        <select
                            value={maintenanceForm.status}
                            onChange={handleChange}
                            id="status"
                            name="status"
                            className="form-select"
                        >
                            <option value="">Select a Status</option>
                            <option value="1">Pending</option>
                            <option value="2">Approved</option>
                            <option value="3">Denied</option>
                            <option value="4">In Progress</option>
                            <option value="5">Complete</option>
                        </select>
                    </div>
                    <div>
                        <label>Resolved</label>
                        <select
                            onChange={handleChange}
                            name="resolved"
                            id="resolved"
                            className="form-select"
                            value={maintenanceForm.resolved}
                        >
                            <option value="">Select one</option>
                            <option value="false">Not yet resolved</option>
                            <option value="true">Resolved</option>
                        </select>
                    </div>
                    <div>
                        <label>Resolved On</label>
                        <input
                            type="date"
                            name="resolved_on"
                            id="resolved_on"
                            value={maintenanceForm.resolved_on ?? ''}
                            onChange={handleChange}
                            className="form-control"
                        />
                    </div>
                    <div>
                        <button type="submit" className="btn btn-primary">
                            Save Changes
                        </button>
                    </div>
                </form>
            </div>
        )
    }
    if (isLoggedIn) {
        return (
            <div>
                <p>
                    We didn't find any properties or maintenance tickets that
                    are associated with your user profile.
                </p>
            </div>
        )
    }
    return (
        <div>
            <p>
                Please log into your property manager or tenant portal to view
                this page.
            </p>
        </div>
    )
}
