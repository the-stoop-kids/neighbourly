// @ts-check
import { useState } from 'react'
import { Navigate } from 'react-router-dom'
import useAuthService from '../hooks/useAuthService'
import TextField from '@mui/material/TextField'
import Button from '@mui/material/Button'
import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'
import Paper from '@mui/material/Paper'
import Link from '@mui/material/Link'


export default function SignInForm() {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const { signin, user, error } = useAuthService()

    async function handleFormSubmit(e) {
        e.preventDefault()
        await signin({ username, password })
    }

    if (user) {
        return <Navigate to="/" />
    }

    return (
        <Container
            sx={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between"
                }}
        >
            <Box>
                <Typography
                    variant="h1"
                >
                    neighbourly
                </Typography>
                <Typography
                    variant="h6"
                >
                    Rental Manager
                </Typography>
            </Box>
            <Box
                component="form"
                sx={{
                    '& .MuiTextField-root': { m: 1, width: '75ch' },
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
                noValidate
                autoComplete="off"
                onSubmit={handleFormSubmit}
            >
                    <Typography
                        variant="h2"
                        sx={{ mb: 4 }}
                    >
                        Log In
                    </Typography>
                    {error && <div className="error">{error.message}</div>}

                    <TextField
                        type="text"
                        required
                        name="username"
                        id="username"
                        label="Username"

                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                        sx={{ mb: 4 }}
                    />
                    <TextField
                        type="text"
                        required
                        name="password"
                        id="password"
                        label="Password"

                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                    <Button
                        type="submit"
                        sx={{ mb: 2 }}
                        size='large'

                    >
                        Sign In
                    </Button>
                    <Link
                        href='forgot-password'
                        underline='none'
                        variant='button'
                        sx={{
                            mb : 2
                        }}
                    >
                        Forgot Password?
                    </Link>
            </Box>
        </Container>
    )
}
