import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import useAuthService from '../hooks/useAuthService'


export default function SignOut() {
    const navigate = useNavigate()
    const { signout } = useAuthService()

    async function handleSignout() {
        await signout()
        navigate('/')
    }

    useEffect(() => {
        handleSignout()
    }, [navigate, signout])
    return null
}
