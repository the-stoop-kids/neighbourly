// @ts-check
import { useState, useEffect } from 'react';
import useAuthService from '../hooks/useAuthService';
import { Link, useNavigate } from 'react-router-dom';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import FormHelperText from '@mui/material/FormHelperText';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';


export default function SignUpForm() {
    const [formData, setFormData] = useState({
        username: '',
        password: '',
        first_name: '',
        last_name: '',
        picture_url: '',
        email: '',
        phone_number: '',
        role: '',
    })

    const { signup, user } = useAuthService()
    const navigate = useNavigate()

    const handleFormChange = (e) => {
        const { name, value } = e.target
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }))
    }

    const handleRegistration = async (e) => {
        e.preventDefault()

        setFormData({
            ...formData,
        })

        await signup(formData)
    }

    useEffect(() => {
        if (user) {
            navigate('/')
        }
    }, [user])

    return (
        <Box
            component="form"
            sx={{
                '& .MuiTextField-root': { m: 1, width: '75ch' },
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
            }}
            noValidate
            autoComplete="off"
            onSubmit={handleRegistration}
        >
            <Typography variant='h2'>Create an account</Typography>
            <TextField
                type="text"
                required
                name="username"
                id="username"
                value={formData.username}
                onChange={handleFormChange}
                label="Username"
                fullWidth
                sx={{ mb: 2 }}
            />
            <TextField
                type="password"
                required
                name="password"
                id="password"
                value={formData.password}
                onChange={handleFormChange}
                label="Password"
                fullWidth
                sx={{ mb: 2 }}
            />
            <TextField
                type="text"
                required
                name="first_name"
                id="first_name"
                value={formData.first_name}
                onChange={handleFormChange}
                label="First Name"
                fullWidth
                sx={{ mb: 2 }}
            />
            <TextField
                type="text"
                required
                name="last_name"
                id="last_name"
                value={formData.last_name}
                onChange={handleFormChange}
                label="Last Name"
                fullWidth
                sx={{ mb: 2 }}
            />
            <TextField
                type="text"
                name="picture_url"
                id="picture_url"
                value={formData.picture_url}
                onChange={handleFormChange}
                label="Picture URL"
                fullWidth
                sx={{ mb: 2 }}
            />
            <TextField
                type="email"
                required
                name="email"
                id="email"
                value={formData.email}
                onChange={handleFormChange}
                label="Email"
                fullWidth
                sx={{ mb: 2 }}
            />
            <TextField
                type="text"
                required
                name="phone_number"
                id="phone_number"
                value={formData.phone_number}
                onChange={handleFormChange}
                label="Phone Number"
                fullWidth
                sx={{ mb: 2 }}
            />
            <Select
                onChange={handleFormChange}
                name="role"
                id="role"
                value={formData.role}
                sx={{ m: 1, width: '75ch' }}
            >
                <MenuItem value="1">Property Manager</MenuItem>
                <MenuItem value="2">Tenant</MenuItem>
            </Select>
            <FormHelperText>Select a Role</FormHelperText>
            <Button
                type="submit"
                sx={{ m: 1, width: '75ch' }}
                fullWidth
                color='inherit'
                variant='contained'
            >
                Register
            </Button>
            <Typography variant='body2'>
                Already have an account?
                <Button component={Link} to='/signin' color='primary'>Log in</Button>
            </Typography>
        </Box>
    )
}
