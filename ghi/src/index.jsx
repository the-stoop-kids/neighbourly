//@ts-check
import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import AuthProvider from './auth/AuthProvider'
import './index.css'
import { GlobalStyles, ThemeProvider, createTheme } from '@mui/material'


const theme = createTheme({
    palette: {
        primary: {
            main: "#2196f3"
        },
    },
    typography: {
        fontFamily: 'Roboto',
        fontWeightLight: 400,
        fontWeightRegular: 500,
        fontWeightMedium: 600,
        fontWeightBold: 700,
    },
})

const BASE_URL = import.meta.env.BASE_URL
if (!BASE_URL) {
    throw new Error('BASE_URL is not defined')
}


const rootElement = document.getElementById('root')
if (!rootElement) {
    throw new Error('root element was not found!')
}

// Log out the environment variables while you are developing and deploying
// This will help debug things
console.table(import.meta.env)

const root = ReactDOM.createRoot(rootElement)
root.render(
    <ThemeProvider theme={theme}>
        <GlobalStyles
            styles={{
                body: {
                    backgroundColor: "#cfd8dc",
                },
            }}
        />
        <React.StrictMode>
            <AuthProvider>
                <App />
            </AuthProvider>
        </React.StrictMode>
    </ThemeProvider>
)
