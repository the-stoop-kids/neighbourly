import React from "react";
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import MiniDrawer from "../nav/SideDrawer";


export default function Layout({ children }) {

    return (
        <Container
            maxWidth="100vw"
            sx={{
                maxHeight: "100vh",
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
            }}
            disableGutters
        >

            {/* Placeholder */}

            <MiniDrawer />

            <Box>
                {children}
            </Box>

        </Container>
    )
}
