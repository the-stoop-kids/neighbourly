//@ts-check
import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import HomeForm from './pages/HomeForm'
import SignUpForm from './auth/SignUpForm'
import SignInForm from './auth/SignInForm'
import SignOut from './auth/SignOut'
import GetAllUsers from './pages/GetAllUsers'
import UserPage from './pages/UserPage'
import EditUserForm from './components/UserEditForm'
import ResetPassword from './components/ResetPassword'
import BillsList from './pages/BillsList'
import NewBillForm from './components/BillForm'
import EditBillform from './components/BillEditForm'
import CommunitiesList from './pages/CommunitiesList'
import CreateCommunity from './components/CreateCommunityForm'
import CommunityEdit from './components/CommunityEdit'
import EventsList from './pages/EventsList'
import EventForm from './components/EventForm'
import EventEdit from './components/EventEdit'
import MaintenanceList from './pages/MaintenanceList'
import MaintenanceForm from './components/MaintenanceForm'
import MaintenanceEdit from './components/MaintenanceEdit'
import ReviewsList from './pages/ReviewsList'
import ReviewForm from './components/ReviewForm'
import EditReviewform from './components/ReviewEditForm'
import PropertiesList from './pages/PropertiesList'
import Layout from './mui/Layout'

const API_HOST = import.meta.env.VITE_API_HOST

if (!API_HOST) {
    throw new Error('VITE_API_HOST is not defined')
}

function App() {
    return (
        <BrowserRouter>
            <Layout>
                <Routes>
                    <Route path="/" element={<HomeForm />} />
                    <Route path="signup" element={<SignUpForm />} />
                    <Route path="signin" element={<SignInForm />} />
                    <Route path="signout" element={<SignOut />} />
                    <Route path="directory" element={<GetAllUsers />} />
                    <Route path="bills" index element={<BillsList />} />
                    <Route path="bills/create" element={<NewBillForm />} />
                    <Route path="bills/:bill_id" element={<EditBillform />} />
                    <Route path="communities" element={<CommunitiesList />} />
                    <Route
                        path="/communities/create"
                        element={<CreateCommunity />}
                    />
                    <Route
                        path="/communities/:communityId"
                        element={<CommunityEdit />}
                    />
                    <Route path="/profile" element={<UserPage />} />
                    <Route path="/profile/edit" element={<EditUserForm />} />
                    <Route
                        path="/profile/passwordreset"
                        element={<ResetPassword />}
                    />
                    <Route path="maintenance" element={<MaintenanceList />} />
                    <Route
                        path="maintenance/create"
                        element={<MaintenanceForm />}
                    />
                    <Route
                        path="maintenance/:maintenance_id"
                        element={<MaintenanceEdit />}
                    />
                    <Route path="events" element={<EventsList />} />
                    <Route path="events/create" element={<EventForm />} />
                    <Route path="events/:event_id" element={<EventEdit />} />
                    <Route path="/reviews" element={<ReviewsList />} />
                    <Route path="/reviews/create" element={<ReviewForm />} />
                    <Route
                        path="reviews/:review_id"
                        element={<EditReviewform />}
                    />
                    <Route path="properties" element={<PropertiesList />} />
                </Routes>
            </Layout>
        </BrowserRouter>
    )
}

export default App
