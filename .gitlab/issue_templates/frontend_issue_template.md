### Description:

This section is a high level overview of what this task includes i.e. a user story.

### Acceptance Criteria:

- [ ] As a { logged in / not logged in } user, I can view { components } when at { some url }.
- [ ] As a { logged in / not logged in } user, I can { some action }.
- [ ] This { renders or redirects } { some page } that does { some action }.
- [ ] If I { fill out or click on } { some form / some button }, on submit, this will send a request to the backend which will do { some action } and returns { something }.
- [ ] This { renders or redirects } { some page } that does { some action }.
- [ ] Search functionality (if applicable) - describe functionality, search terms/criteria, and return values

### Definition of Done:

- [ ] Documentation (if needed)
- [ ] Source code
- [ ] Unit test(s) (if needed)
- [ ] Passing CI/CD pipeline(s)
- [ ] Approved, linked issue to merge request, and initiated merge request.
- [ ] Verified console clear of issues/errors.

### Notes (Optional but encouraged):

This section can include articles and other resources that might help the developer.
Changes/Modifications made. (if any)
