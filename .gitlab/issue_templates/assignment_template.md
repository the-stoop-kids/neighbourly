### Description/Feature:

As a { user }, I want to { some action }.

### Items completed for Feature:

- [ ] As a new user, I can do -> { some action }
- [ ] This sends a request to the backend which -> { some action }
- [ ] 200 response when request sent via localhost:8000/docs
- [ ] Verify creation of request in database using beekeeper (if applicable)
- [ ] Passes flake8 format tests

### Items needed for Feature Completion:

- [ ] Documentation (if needed)
- [ ] Source code
- [ ] Unit test(s) (if needed)
- [ ] Approved and completed merge request(s)

### Notes:

This section can include articles and other resources that might help the developer.