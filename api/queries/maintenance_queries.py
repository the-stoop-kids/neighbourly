import os
import psycopg  # type: ignore
from psycopg_pool import ConnectionPool  # type: ignore
from utils.exceptions import UserDatabaseException
from models.maintenance import (
    MaintenanceIn,
    MaintenanceOut,
    MaintenanceEdit,
    FEMaintenanceOut,
    SimpleMaintenanceOut,
)
from models.errors import Error
from typing import List, Optional, Union

DATABASE_URL = os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")

pool = ConnectionPool(DATABASE_URL)


class MaintenanceQueries:
    def create_maintenance_request(
        self,
        maintenance: MaintenanceIn
    ) -> SimpleMaintenanceOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        INSERT INTO maintenance (
                            issue,
                            attempts,
                            outcome,
                            assigned_personnel,
                            status,
                            maker,
                            property_id
                        ) VALUES (
                            %s, %s, %s, %s, %s, %s, %s
                        )
                        RETURNING *;
                        """,
                        [
                            maintenance.issue,
                            maintenance.attempts,
                            maintenance.outcome,
                            maintenance.assigned_personnel,
                            maintenance.status,
                            maintenance.maker,
                            maintenance.property_id
                        ],
                    )
                    request = result.fetchone()
                    return SimpleMaintenanceOut(
                        id=request[0],
                        issue=request[1],
                        attempts=request[2],
                        outcome=request[3],
                        assigned_personnel=request[4],
                        status=request[5],
                        resolved=request[6],
                        resolved_on=request[7],
                        maker=request[8],
                        created_on=request[9]
                    )
        except psycopg.Error as e:
            print(e)
            raise UserDatabaseException(
                "Could not create Maintenance Request."
            )

    def get_requests(self) -> Optional[List[MaintenanceOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        SELECT
                            m.id,
                            m.issue,
                            m.attempts,
                            m.outcome,
                            m.assigned_personnel,
                            s.status,
                            m.resolved,
                            m.resolved_on,
                            m.maker,
                            u.first_name,
                            u.last_name,
                            p.property_manager,
                            m.property_id,
                            p.street,
                            p.city,
                            p.state,
                            p.zipcode,
                            m.created_on
                        FROM
                            maintenance m
                            INNER JOIN status s ON m.status = s.id
                            INNER JOIN users u ON m.maker = u.id
                            INNER JOIN properties p ON m.property_id = p.id;
                        """,
                    )
                    requests = cur.fetchall()
                    return [
                        MaintenanceOut(
                            id=request[0],
                            issue=request[1],
                            attempts=request[2],
                            outcome=request[3],
                            assigned_personnel=request[4],
                            status=request[5],
                            resolved=request[6],
                            resolved_on=request[7],
                            maker=request[8],
                            first_name=request[9],
                            last_name=request[10],
                            property_manager=request[11],
                            property_id=request[12],
                            street=request[13],
                            city=request[14],
                            state=request[15],
                            zipcode=request[16],
                            created_on=request[17],
                        )
                        for request in requests
                    ]
        except Exception as e:
            print(e)

    def get_request_by_id(
        self,
        maintenance_id: int
    ) -> Optional[Union[FEMaintenanceOut, Error]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        SELECT
                            m.id,
                            m.issue,
                            m.attempts,
                            m.outcome,
                            m.assigned_personnel,
                            m.status,
                            m.resolved,
                            m.resolved_on,
                            m.maker,
                            m.created_on,
                            m.property_id,
                            p.property_manager,
                            (
                            u.first_name || ' ' ||
                            u.last_name
                            ) AS tenant_name
                        FROM
                            maintenance m
                            INNER JOIN status s ON m.status = s.id
                            INNER JOIN users u ON m.maker = u.id
                            INNER JOIN properties p ON m.property_id = p.id
                        WHERE m.id = (%s)
                        """,
                        [maintenance_id]
                    )
                    requests = result.fetchone()
                    if requests is None:
                        return Error(
                            message="No request with that ID was found."
                        )
                    return FEMaintenanceOut(
                        id=requests[0],
                        issue=requests[1],
                        attempts=requests[2],
                        outcome=requests[3],
                        assigned_personnel=requests[4],
                        status=requests[5],
                        resolved=requests[6],
                        resolved_on=requests[7],
                        maker=requests[8],
                        created_on=requests[9],
                        property_id=requests[10],
                        property_manager=requests[11],
                        tenant_name=requests[12]
                        )
        except Exception as e:
            print(e)

    def update(
        self,
        maintenance_id: int,
        maintenance: MaintenanceEdit
    ) -> Optional[SimpleMaintenanceOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        UPDATE maintenance
                        SET issue = %s
                            ,   attempts = %s
                            ,   outcome = %s
                            ,   assigned_personnel = %s
                            ,   status = %s
                            ,   resolved = %s
                            ,   resolved_on = %s
                        WHERE id = %s
                        RETURNING *
                        """,
                        [
                            maintenance.issue,
                            maintenance.attempts,
                            maintenance.outcome,
                            maintenance.assigned_personnel,
                            maintenance.status,
                            maintenance.resolved,
                            maintenance.resolved_on,
                            maintenance_id,
                        ]
                    )
                    requests = result.fetchone()
                    return SimpleMaintenanceOut(
                        id=requests[0],
                        issue=requests[1],
                        attempts=requests[2],
                        outcome=requests[3],
                        assigned_personnel=requests[4],
                        status=requests[5],
                        resolved=requests[6],
                        resolved_on=requests[7],
                        maker=requests[8],
                        created_on=requests[9]
                    )
        except Exception as e:
            print(e)

    def delete(self, maintenance_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE
                        FROM maintenance
                        WHERE id = %s
                        """,
                        [maintenance_id]
                    )
                    return {"message": "Deleted maintenance request."}
        except Exception as e:
            print(e)
