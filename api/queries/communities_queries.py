import os
import psycopg  # type: ignore
from psycopg_pool import ConnectionPool  # type: ignore
from utils.exceptions import UserDatabaseException
from models.communities import (
    CommunitiesIn,
    CommunitiesOut,
    CommunitiesEdit,
    GetCommunitiesOut
)
from models.errors import Error
from typing import Union, Optional, List

DATABASE_URL = os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")

pool = ConnectionPool(DATABASE_URL)


class CommunityQueries:
    def create_community(self, community: CommunitiesIn) -> CommunitiesOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        INSERT INTO communities (
                            title,
                            owner,
                            description,
                            city,
                            state,
                            zipcode,
                            rating
                        ) VALUES (
                            %s, %s, %s, %s, %s, %s, %s
                        )
                        RETURNING *;
                        """,
                        [
                            community.title,
                            community.owner,
                            community.description,
                            community.city,
                            community.state,
                            community.zipcode,
                            community.rating,
                        ],
                    )
                    record = result.fetchone()
                    return CommunitiesOut(
                        id=record[0],
                        title=record[1],
                        owner=record[2],
                        description=record[3],
                        city=record[4],
                        state=record[5],
                        zipcode=record[6],
                        rating=record[7],
                        created_on=record[8]
                    )
        except psycopg.Error as e:
            print(e)
            raise UserDatabaseException(
                "Could not create community."
            )

    def get_communities(self) -> Optional[List[GetCommunitiesOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        SELECT
                            c.id,
                            c.title,
                            u.first_name,
                            u.last_name,
                            c.description,
                            c.city,
                            c.state,
                            c.zipcode,
                            c.rating,
                            c.created_on
                        FROM
                            communities c
                        JOIN
                            users u ON c.owner = u.id;
                        """,
                    )
                    communities = result.fetchall()
                    return [
                        GetCommunitiesOut(
                            id=community[0],
                            title=community[1],
                            first_name=community[2],
                            last_name=community[3],
                            description=community[4],
                            city=community[5],
                            state=community[6],
                            zipcode=community[7],
                            rating=community[8],
                            created_on=community[9],
                        ) for community in communities
                    ]
        except psycopg.Error as e:
            print(e)

    def get_community_by_id(
        self,
        community_id: int
    ) -> Optional[Union[CommunitiesOut, Error]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        SELECT
                            c.id,
                            c.title,
                            u.first_name,
                            u.last_name,
                            c.description,
                            c.city,
                            c.state,
                            c.zipcode,
                            c.rating,
                            c.created_on
                        FROM
                            communities c
                        JOIN
                            users u ON c.owner = u.id;
                        WHERE id = (%s)
                        """,
                        [community_id]
                    )
                    record = result.fetchone()
                    if record is None:
                        return Error(
                            message="No community with that ID was found."
                        )
                    return GetCommunitiesOut(
                        id=record[0],
                        title=record[1],
                        first_name=record[2],
                        last_name=record[3],
                        description=record[4],
                        city=record[5],
                        state=record[6],
                        zipcode=record[7],
                        rating=record[8],
                        created_on=record[9]
                    )
        except Exception as e:
            print(e)

    def update(
            self,
            community_id: int,
            community: CommunitiesEdit
    ) -> Optional[CommunitiesOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        UPDATE communities
                        SET title = %s
                            , owner = %s
                            , description = %s
                            , city = %s
                            , state = %s
                            , zipcode = %s
                            , rating = %s
                        WHERE id = %s
                        RETURNING *
                        """,
                        [
                            community.title,
                            community.owner,
                            community.description,
                            community.city,
                            community.state,
                            community.zipcode,
                            community.rating,
                            community_id
                        ]
                    )
                    record = result.fetchone()
                    return CommunitiesOut(
                        id=record[0],
                        title=record[1],
                        owner=record[2],
                        description=record[3],
                        city=record[4],
                        state=record[5],
                        zipcode=record[6],
                        rating=record[7],
                        created_on=record[8]
                    )
        except Exception as e:
            print(e)

    def delete(self, community_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE
                        FROM communities
                        WHERE id = %s
                        """,
                        [community_id]
                    )
                    return {"message": "Deleted community."}
        except Exception as e:
            print(e)
