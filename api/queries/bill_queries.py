import os
from psycopg_pool import ConnectionPool  # type: ignore
from models.errors import Error
from models.bills import BillIn, BillOut
from typing import List, Union, Optional


DATABASE_URL = os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")


pool = ConnectionPool(DATABASE_URL)


class BillRepository:
    def create_bill(self, bill: BillIn) -> Optional[BillOut]:
        """
        Adds a new bill to the bills table in the DB upon valid request.
        All rows but property_id MUST be defined.
        Returns an instance of BillOut as valid response.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        INSERT INTO bills (
                            tenant,
                            property_manager,
                            link,
                            bill_name,
                            bill_type,
                            due_date,
                            amount,
                            complete,
                            property_id
                        ) VALUES (
                            %s, %s, %s, %s, %s, %s, %s, %s, %s
                        )
                        RETURNING *;
                        """,
                        [
                            bill.tenant,
                            bill.property_manager,
                            bill.link,
                            bill.bill_name,
                            bill.bill_type,
                            bill.due_date,
                            bill.amount,
                            bill.complete,
                            bill.property_id
                        ],
                    )
                    record = result.fetchone()
                    return BillOut(
                        id=record[0],
                        tenant=record[1],
                        property_manager=record[2],
                        link=record[3],
                        bill_name=record[4],
                        bill_type=record[5],
                        due_date=record[6],
                        amount=record[7],
                        complete=record[8],
                        property_id=record[9],
                        created_on=record[10],
                    )
        except Exception as e:
            print(e)

    def get_bill_by_id(self, bill_id: int) -> Optional[Union[BillOut, Error]]:
        """
        This function will return a Bill from the Bills Table.
        Request will use the specific id and return an instance
        of BillOut with the same id.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        SELECT *
                        FROM bills
                        WHERE id = (%s)
                        """,
                        [bill_id]
                    )
                    record = result.fetchone()
                    if record is None:
                        return Error(
                            message="No bill with that ID was found."
                        )
                    return BillOut(
                        id=record[0],
                        tenant=record[1],
                        property_manager=record[2],
                        link=record[3],
                        bill_name=record[4],
                        bill_type=record[5],
                        due_date=record[6],
                        amount=record[7],
                        complete=record[8],
                        property_id=record[9],
                        created_on=record[10],
                    )
        except Exception as e:
            print(e)

    def get_all_bills(self) -> Optional[List[BillOut]]:
        """
        Function will query the DB to get a list of all bills as BillOuts.
        This can further be filtered by tenant_id, id, or property_manager id.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        SELECT *
                        FROM bills
                        """
                    )
                    bills = result.fetchall()
                    return [
                        BillOut(
                            id=record[0],
                            tenant=record[1],
                            property_manager=record[2],
                            link=record[3],
                            bill_name=record[4],
                            bill_type=record[5],
                            due_date=record[6],
                            amount=record[7],
                            complete=record[8],
                            property_id=record[9],
                            created_on=record[10],
                        ) for record in bills
                    ]
        except Exception as e:
            print(e)

    def update(self, bill_id: int, bill: BillIn) -> Optional[BillOut]:
        """
        Function will grab a bill by it's id.
        A valid request will be sent to update specified rows.
        Returns an instance of BillOut with the updated data.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        UPDATE bills
                        SET link = %s
                            , bill_name = %s
                            , bill_type = %s
                            , due_date = %s
                            , amount = %s
                            , complete = %s
                        WHERE id = %s
                        RETURNING *
                        """,
                        [
                            bill.link,
                            bill.bill_name,
                            bill.bill_type,
                            bill.due_date,
                            bill.amount,
                            bill.complete,
                            bill_id
                        ]
                    )
                    record = result.fetchone()
                    return BillOut(
                        id=record[0],
                        tenant=record[1],
                        property_manager=record[2],
                        link=record[3],
                        bill_name=record[4],
                        bill_type=record[5],
                        due_date=record[6],
                        amount=record[7],
                        complete=record[8],
                        property_id=record[9],
                        created_on=record[10],
                    )
        except Exception as e:
            print(e)

    def delete(self, bill_id: int):
        """
        This function will remove an instance of BillOut(the bill)
        using its id for reference.
        Upon successful delete, it will return "Deleted bill".
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE
                        FROM bills
                        WHERE id = %s
                        """,
                        [bill_id]
                    )
                    return {"message": "Deleted bill."}
        except Exception as e:
            print(e)
