import os
from psycopg_pool import ConnectionPool  # type: ignore
from models.errors import Error
from models.reviews import FrontEndReviewOut, ReviewIn, ReviewEdit, ReviewOut
from typing import List, Optional, Union


DATABASE_URL = os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")


pool = ConnectionPool(DATABASE_URL)


class ReviewRepository:
    def create(self, review: ReviewIn) -> Optional[ReviewOut]:
        """
        Adds a new review to the reviews table in the DB upon valid request.
        All rows MUST be defined.
        Returns an instance of ReviewOut as valid response.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        INSERT INTO reviews (
                            title,
                            experience,
                            reviewer,
                            rating,
                            property_id
                        ) VALUES (
                            %s, %s, %s, %s, %s
                        )
                        RETURNING *;
                        """,
                        [
                            review.title,
                            review.experience,
                            review.reviewer,
                            review.rating,
                            review.property_id,
                        ],
                    )
                    record = result.fetchone()
                    return ReviewOut(
                        id=record[0],
                        title=record[1],
                        experience=record[2],
                        reviewer=record[3],
                        rating=record[4],
                        property_id=record[5],
                        created_on=record[6],
                    )
        except Exception as e:
            print(e)

    def get_review_by_id(
        self,
        review_id: int
    ) -> Optional[Union[ReviewOut, Error]]:
        """
        This function will return a Review from the Reviews Table.
        Request will use the specific id and return an instance
        of ReviewOut with the same id.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        SELECT *
                        FROM reviews
                        WHERE id = (%s)
                        """,
                        [review_id]
                    )
                    record = result.fetchone()
                    if record is None:
                        return Error(
                            message="No review with that ID was found."
                        )
                    return ReviewOut(
                        id=record[0],
                        title=record[1],
                        experience=record[2],
                        reviewer=record[3],
                        rating=record[4],
                        property_id=record[5],
                        created_on=record[6],
                    )
        except Exception as e:
            print(e)

    def get_all_reviews(self) -> Optional[List[FrontEndReviewOut]]:
        """
        Function will query the DB to get a list of all reviews as ReviewOuts.
        This can further be filtered by any column on the table.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        SELECT
                            r.id,
                            r.title,
                            r.experience,
                            r.reviewer,
                            (
                            u.first_name || ' ' ||
                            u.last_name
                            ) AS reviewer_name,
                            r.rating,
                            CASE
                                WHEN r.property_id IS NULL
                                    THEN 'No property associated.'
                                ELSE (
                                    p.street || ', ' ||
                                    p.city || ', ' ||
                                    p.state || ', ' ||
                                    p.zipcode
                                )
                            END AS property_id,
                        r.created_on
                        FROM reviews r
                        INNER JOIN users u ON r.reviewer = u.id
                        FULL OUTER JOIN properties p ON r.property_id = p.id
                        WHERE r.id IS NOT NULL
                        """
                    )
                    reviews = result.fetchall()
                    return [
                        FrontEndReviewOut(
                            id=record[0],
                            title=record[1],
                            experience=record[2],
                            reviewer=record[3],
                            reviewer_name=record[4],
                            rating=record[5],
                            property_id=record[6],
                            created_on=record[7],
                        ) for record in reviews
                    ]
        except Exception as e:
            print(e)

    def update(
            self,
            review_id: int,
            review: ReviewEdit
    ) -> Optional[ReviewOut]:
        """
        Function will grab a review by its id.
        A valid request will be sent to update specified rows.
        Returns an instance of ReviewOut with the updated data.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        UPDATE reviews
                        SET title = %s
                            , experience = %s
                            , rating = %s
                        WHERE id = %s
                        RETURNING *
                        """,
                        [
                            review.title,
                            review.experience,
                            review.rating,
                            review_id
                        ]
                    )
                    record = result.fetchone()
                    return ReviewOut(
                        id=record[0],
                        title=record[1],
                        experience=record[2],
                        reviewer=record[3],
                        rating=record[4],
                        property_id=record[5],
                        created_on=record[6],
                    )
        except Exception as e:
            print(e)

    def delete(self, review_id: int):
        """
        This function will remove an instance of ReviewOut(the review)
        using its id for reference.
        Upon successful delete, it will return "Deleted review".
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE
                        FROM reviews
                        WHERE id = %s
                        """,
                        [review_id]
                    )
                    return {"message": "Deleted review."}
        except Exception as e:
            print(e)
