import os
from typing import List, Optional, Union
from psycopg_pool import ConnectionPool  # type: ignore
from models.errors import Error
from models.properties import PropertyIn, PropertyOut, PropertyEdit


DATABASE_URL = os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")

pool = ConnectionPool(DATABASE_URL)


class PropertyRepository:
    def create(self, properties: PropertyIn) -> Optional[PropertyOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        INSERT INTO properties (
                            street,
                            city,
                            state,
                            zipcode,
                            beds,
                            baths,
                            property_manager,
                            tenant,
                            community
                        ) VALUES (
                            %s, %s, %s, %s, %s, %s, %s, %s, %s
                        )
                        RETURNING *;
                        """,
                        [
                            properties.street,
                            properties.city,
                            properties.state,
                            properties.zipcode,
                            properties.beds,
                            properties.baths,
                            properties.property_manager,
                            properties.tenant,
                            properties.community,
                        ],
                    )
                    record = result.fetchone()
                    return PropertyOut(
                        id=record[0],
                        street=record[1],
                        city=record[2],
                        state=record[3],
                        zipcode=record[4],
                        beds=record[5],
                        baths=record[6],
                        property_manager=record[7],
                        tenant=record[8],
                        community=record[9],
                        created_on=record[10]
                    )
        except Exception as e:
            print(e)

    def get_property_by_id(
        self,
        property_id: int
    ) -> Optional[Union[PropertyOut, Error]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        SELECT *
                        FROM properties
                        WHERE id = (%s)
                        """,
                        [property_id]
                    )
                    record = result.fetchone()
                    if record is None:
                        return Error(
                            message="No property with that ID was found."
                        )
                    return PropertyOut(
                        id=record[0],
                        street=record[1],
                        city=record[2],
                        state=record[3],
                        zipcode=record[4],
                        beds=record[5],
                        baths=record[6],
                        property_manager=record[7],
                        tenant=record[8],
                        community=record[9],
                        created_on=record[10]
                    )
        except Exception as e:
            print(e)

    def get_properties(self) -> Optional[List[PropertyOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        SELECT *
                        FROM properties
                        """
                    )
                    properties = result.fetchall()
                    return [
                        PropertyOut(
                            id=record[0],
                            street=record[1],
                            city=record[2],
                            state=record[3],
                            zipcode=record[4],
                            beds=record[5],
                            baths=record[6],
                            property_manager=record[7],
                            tenant=record[8],
                            community=record[9],
                            created_on=record[10],
                        ) for record in properties
                    ]
        except Exception as e:
            print(e)

    def delete_property(self, property_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE
                        FROM properties
                        WHERE id = %s
                        """,
                        [property_id]
                    )
                    return {"message": "Deleted property."}
        except Exception as e:
            print(e)

    def update(
        self,
        property_id: int,
        propertea: PropertyEdit
    ) -> Optional[PropertyOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        UPDATE properties
                        SET beds = %s
                            , baths = %s
                            , property_manager = %s
                            , tenant = %s
                            , community = %s
                        WHERE id = %s
                        RETURNING *
                        """,
                        [
                            propertea.beds,
                            propertea.baths,
                            propertea.property_manager,
                            propertea.tenant,
                            propertea.community,
                            property_id
                        ]
                    )
                    record = result.fetchone()
                    return PropertyOut(
                        id=record[0],
                        street=record[1],
                        city=record[2],
                        state=record[3],
                        zipcode=record[4],
                        beds=record[5],
                        baths=record[6],
                        property_manager=record[7],
                        tenant=record[8],
                        community=record[9],
                        created_on=record[10],
                    )
        except Exception as e:
            print(e)
