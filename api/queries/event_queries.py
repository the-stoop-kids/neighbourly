import os
import psycopg  # type: ignore
from psycopg_pool import ConnectionPool  # type: ignore
from utils.exceptions import UserDatabaseException
from models.events import (
    EventIn,
    EventOut,
    EventEdit,
    GetEventOut,
)
from models.errors import Error
from typing import Union, Optional, List

DATABASE_URL = os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")

pool = ConnectionPool(DATABASE_URL)


class EventQueries:
    def create(self, event: EventIn) -> EventOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        INSERT INTO events (
                            title,
                            description,
                            location,
                            event_date,
                            maker,
                            category
                        ) VALUES (
                            %s, %s, %s, %s, %s, %s
                        )
                        RETURNING *;
                        """,
                        [
                            event.title,
                            event.description,
                            event.location,
                            event.event_date,
                            event.maker,
                            event.category
                        ],
                    )
                    record = result.fetchone()
                    return EventOut(
                        id=record[0],
                        title=record[1],
                        description=record[2],
                        location=record[3],
                        event_date=record[4],
                        maker=record[5],
                        category=record[6],
                        created_on=record[7],
                    )
        except psycopg.Error as e:
            print(e)
            raise UserDatabaseException(
                f"Could not create event. {event}"
            )

    def get_all_events(self) -> Optional[List[GetEventOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        SELECT
                            e.id,
                            e.title,
                            e.description,
                            e.location,
                            e.event_date,
                            e.maker,
                            (
                            u.first_name || ' ' ||
                            u.last_name
                            ) AS maker_name,
                            c.category,
                            e.created_on
                        FROM events e
                            INNER JOIN users u ON e.maker = u.id
                            INNER JOIN category c ON e.category = c.id;
                        """,
                    )
                    events = cur.fetchall()
                    return [
                        GetEventOut(
                            id=event[0],
                            title=event[1],
                            description=event[2],
                            location=event[3],
                            event_date=event[4],
                            maker=event[5],
                            maker_name=event[6],
                            category=event[7],
                            created_on=event[8],
                        )
                        for event in events
                    ]
        except psycopg.Error as e:
            print(e)
            raise UserDatabaseException("Error getting events.")

    def get_event_by_id(
        self,
        event_id: int
    ) -> Optional[Union[EventOut, Error]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        SELECT *
                        FROM events
                        WHERE id = (%s)
                        """,
                        [event_id]
                    )
                    record = result.fetchone()
                    if record is None:
                        return Error(
                            message="No event with that ID was found."
                        )
                    return EventOut(
                        id=record[0],
                        title=record[1],
                        description=record[2],
                        location=record[3],
                        event_date=record[4],
                        maker=record[5],
                        category=record[6],
                        created_on=record[7],
                    )
        except Exception as e:
            print(e)

    def delete(self, event_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE
                        FROM events
                        WHERE id = %s
                        """,
                        [event_id]
                    )
                    return {"message": "Deleted event."}
        except Exception as e:
            print(e)

    def update(self, event_id: int, event: EventEdit) -> Optional[EventOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        UPDATE events
                        SET location = %s
                            , event_date = %s
                        WHERE id = %s
                        RETURNING *
                        """,
                        [
                            event.location,
                            event.event_date,
                            event_id
                        ]
                    )
                    record = result.fetchone()
                    return EventOut(
                        id=record[0],
                        title=record[1],
                        description=record[2],
                        location=record[3],
                        event_date=record[4],
                        maker=record[5],
                        category=record[6],
                        created_on=record[7],
                    )
        except Exception as e:
            print(e)
