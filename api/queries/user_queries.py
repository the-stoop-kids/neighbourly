"""
Database Queries for Users
"""
import os
import psycopg
from psycopg_pool import ConnectionPool
from typing import Optional, List, Union
from models.users import (
    UserIn,
    UserOut,
    UserOutWithPw,
    UserEdit,
    UserList,
)
from utils.exceptions import UserDatabaseException

DATABASE_URL = os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")

pool = ConnectionPool(DATABASE_URL)


class UserQueries:
    """
    Class containing queries for the Users table

    Can be dependency injected into a route like so

    def my_route(userQueries: UserQueries = Depends()):
        # Here you can call any of the functions to query the DB
    """

    def get_by_username(self, username: str) -> Union[UserOutWithPw, dict]:
        """
        Gets a user from the database by username

        Returns None if the user isn't found
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                            SELECT
                                *
                            FROM users
                            WHERE username = %s
                            """,
                        [username],
                    )
                    record = result.fetchone()
                    if not record:
                        return {
                            "message": "Couldn't find user with that username."
                        }
                    return UserOutWithPw(
                        id=record[0],
                        username=record[1],
                        hashed_password=record[2],
                        first_name=record[3],
                        last_name=record[4],
                        picture_url=record[5],
                        email=record[6],
                        phone_number=record[7],
                        role=record[8],
                        created_on=record[9],
                    )

        except psycopg.Error as e:
            print(e)
            raise UserDatabaseException(f"Error getting user {username}")

    def get_all_users(self) -> Optional[List[UserList]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        SELECT *
                        FROM users
                        """
                    )
                    users = result.fetchall()
                    return [
                        UserList(
                            id=user[0],
                            username=user[1],
                            first_name=user[3],
                            last_name=user[4],
                            email=user[6],
                            phone_number=user[7],
                            role=user[8],
                        ) for user in users
                    ]
        except Exception as e:
            print(e)

    def get_by_id(self, id: int) -> Union[UserOut, dict]:
        """
        Gets a user from the database by user id

        Returns None if the user isn't found
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                            SELECT
                                *
                            FROM users
                            WHERE id = %s
                            """,
                        [id],
                    )
                    record = result.fetchone()
                    if not record:
                        return {
                            "message": "Could not find a user with that id."
                        }
                    return UserOut(
                        id=record[0],
                        username=record[1],
                        first_name=record[3],
                        last_name=record[4],
                        picture_url=record[5],
                        email=record[6],
                        phone_number=record[7],
                        role=record[8],
                        created_on=record[9],
                    )
        except psycopg.Error as e:
            print(e)
            raise UserDatabaseException(f"Error getting user with id {id}")

    def create_user(
        self,
        user: UserIn,
        hashed_password: str
    ) -> Optional[UserOut]:
        """
        Creates a new user in the database

        Raises a UserInsertionException if creating the user fails
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        INSERT INTO users (
                            username,
                            hashed_password,
                            first_name,
                            last_name,
                            picture_url,
                            email,
                            phone_number,
                            role
                        ) VALUES (
                            %s, %s, %s, %s, %s, %s, %s, %s
                        )
                        RETURNING *;
                        """,
                        [
                            user.username,
                            hashed_password,
                            user.first_name,
                            user.last_name,
                            user.picture_url,
                            user.email,
                            user.phone_number,
                            user.role,
                        ],
                    )
                    record = result.fetchone()
                    return UserOut(
                        id=record[0],
                        username=record[1],
                        first_name=record[3],
                        last_name=record[4],
                        picture_url=record[5],
                        email=record[6],
                        phone_number=record[7],
                        role=record[8],
                        created_on=record[9],
                    )
        except Exception as e:
            print(e)

    def delete(self, id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE
                        FROM users
                        WHERE id = %s
                        """,
                        [id]
                    )
                    return {"message": "User deleted."}
        except Exception as e:
            print(e)

    def update_user(
        self,
        user_id: int,
        user: UserEdit
    ) -> UserOut:  # type: ignore
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        UPDATE users
                        SET username = %s
                            , first_name = %s
                            , last_name = %s
                            , picture_url = %s
                            , email = %s
                            , phone_number = %s
                            , role = %s
                        WHERE id = %s
                        RETURNING *
                        """,
                        [
                            user.username,
                            user.first_name,
                            user.last_name,
                            user.picture_url,
                            user.email,
                            user.phone_number,
                            user.role,
                            user_id
                        ]
                    )
                    record = result.fetchone()
                    return UserOut(
                        id=record[0],
                        username=record[1],
                        first_name=record[3],
                        last_name=record[4],
                        picture_url=record[5],
                        email=record[6],
                        phone_number=record[7],
                        role=record[8],
                        created_on=record[9],
                    )
        except Exception as e:
            print(e)

    def reset_password(
        self,
        user_id: int,
        hashed_password: str,
    ) -> Optional[UserOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        UPDATE users
                        SET hashed_password = %s
                        WHERE id = %s
                        RETURNING *
                        """,
                        [
                            hashed_password,
                            user_id
                        ]
                    )
                    record = result.fetchone()
                    return UserOut(
                        id=record[0],
                        username=record[1],
                        first_name=record[3],
                        last_name=record[4],
                        picture_url=record[5],
                        email=record[6],
                        phone_number=record[7],
                        role=record[8],
                        created_on=record[9],
                    )
        except Exception as e:
            print(e)
