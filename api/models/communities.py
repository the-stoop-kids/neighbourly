from pydantic import BaseModel
from datetime import datetime


class CommunitiesIn(BaseModel):
    title: str
    owner: int
    description: str
    city: str
    state: str
    zipcode: int
    rating: int


class GetCommunitiesOut(BaseModel):
    id: int
    title: str
    first_name: str
    last_name: str
    description: str
    city: str
    state: str
    zipcode: int
    rating: int
    created_on: datetime


class CommunitiesOut(BaseModel):
    id: int
    title: str
    owner: int
    description: str
    city: str
    state: str
    zipcode: int
    rating: int
    created_on: datetime


class CommunitiesEdit(BaseModel):
    title: str
    owner: int
    description: str
    city: str
    state: str
    zipcode: int
    rating: int
