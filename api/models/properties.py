from pydantic import BaseModel
from datetime import datetime


class PropertyIn(BaseModel):
    street: str
    city: str
    state: str
    zipcode: int
    beds: int
    baths: int | float
    property_manager: int
    tenant: int | None = None
    community: int | None = None


class PropertyOut(BaseModel):
    id: int
    street: str
    city: str
    state: str
    zipcode: int
    beds: int
    baths: int | float
    property_manager: int
    tenant: int | None = None
    community: int | None = None
    created_on: datetime


class PropertyEdit(BaseModel):
    beds: int
    baths: int | float
    property_manager: int
    tenant: int | None = None
    community: int | None = None
