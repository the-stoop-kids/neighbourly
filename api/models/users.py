"""
Pydantic Models for Users.
"""
from pydantic import BaseModel
from datetime import datetime


class UserIn(BaseModel):
    """
    Represents the parameters needed to create a new user
    """

    username: str
    password: str
    first_name: str
    last_name: str
    picture_url: str
    email: str
    phone_number: int
    role: int


class UserLogin(BaseModel):
    username: str
    password: str


class UserPassword(BaseModel):
    username: str
    password: str
    password2: str


class UserList(BaseModel):
    id: int
    username: str
    first_name: str
    last_name: str
    email: str
    phone_number: int
    role: int


class UserOut(BaseModel):
    """
    Represents a user, with the password not included
    """

    id: int
    username: str
    first_name: str
    last_name: str
    picture_url: str
    email: str
    phone_number: int
    role: int
    created_on: datetime


class UserOutWithPw(BaseModel):
    """
    Represents a user with password included
    """

    id: int
    username: str
    hashed_password: str
    first_name: str
    last_name: str
    picture_url: str
    email: str
    phone_number: int
    role: int
    created_on: datetime


class UserEdit(BaseModel):
    """
    Represents a request body with editable column data.
    """
    username: str
    first_name: str
    last_name: str
    picture_url: str
    email: str
    phone_number: int
    role: int


class UserResponse(BaseModel):
    """
    For auth purposes.
    """
    id: int
    username: str
