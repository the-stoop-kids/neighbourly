from pydantic import BaseModel
from datetime import date, datetime


class BillIn(BaseModel):
    tenant: int
    property_manager: int
    link: str
    bill_name: str
    bill_type: str
    due_date: date
    amount: float
    complete: bool
    property_id: int


class BillOut(BaseModel):
    id: int
    tenant: int
    property_manager: int
    link: str
    bill_name: str
    bill_type: str
    due_date: date
    amount: float
    complete: bool
    property_id: int
    created_on: datetime
