from pydantic import BaseModel
from datetime import date, datetime


class EventIn(BaseModel):
    title: str
    description: str
    location: str
    event_date: date
    maker: int
    category: int


class EventOut(BaseModel):
    id: int
    title: str
    description: str
    location: str
    event_date: date
    maker: int
    category: int
    created_on: datetime


class GetEventOut(BaseModel):
    id: int
    title: str
    description: str
    location: str
    event_date: date
    maker: int
    maker_name: str
    category: str
    created_on: datetime


class EventEdit(BaseModel):
    location: str
    event_date: date
