from pydantic import BaseModel
from datetime import datetime


class ReviewIn(BaseModel):
    title: str
    experience: str
    reviewer: int
    rating: int
    property_id: int | None = None


class ReviewEdit(BaseModel):
    title: str
    experience: str
    rating: int


class ReviewOut(BaseModel):
    id: int
    title: str
    experience: str
    reviewer: int
    rating: int
    property_id: int | None = None
    created_on: datetime


class FrontEndReviewOut(BaseModel):
    id: int
    title: str
    experience: str
    reviewer: int
    reviewer_name: str
    rating: int
    property_id: int | str
    created_on: datetime
