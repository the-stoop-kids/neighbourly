from pydantic import BaseModel
from datetime import date, datetime


class MaintenanceIn(BaseModel):
    issue: str
    attempts: str
    outcome: str
    assigned_personnel: str
    status: int
    maker: int
    property_id: int


class FEMaintenanceOut(BaseModel):
    id: int
    issue: str
    attempts: str
    outcome: str
    assigned_personnel: str | None = None
    status: int
    resolved: bool
    resolved_on: date | None = None
    maker: int
    created_on: datetime
    property_id: int
    property_manager: int
    tenant_name: str


class SimpleMaintenanceOut(BaseModel):
    id: int
    issue: str
    attempts: str
    outcome: str
    assigned_personnel: str | None = None
    status: int
    resolved: bool
    resolved_on: date | None = None
    maker: int
    created_on: datetime


class MaintenanceEdit(BaseModel):
    issue: str
    attempts: str
    outcome: str
    assigned_personnel: str
    status: int
    resolved: bool
    resolved_on: date | None = None


class MaintenanceOut(BaseModel):
    id: int
    issue: str
    attempts: str
    outcome: str
    assigned_personnel: str
    status: str
    resolved: bool
    resolved_on: date | None = None
    maker: int
    first_name: str
    last_name: str
    property_manager: int
    property_id: int
    street: str
    city: str
    state: str
    zipcode: int
    created_on: datetime
