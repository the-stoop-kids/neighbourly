from fastapi import (  # type: ignore
    Depends,
    Response,
    APIRouter,
    HTTPException,
    status,
)

from models.properties import (
    PropertyIn,
    PropertyOut,
    PropertyEdit,
)

from models.errors import Error
from queries.properties_queries import PropertyRepository
from utils.authentication import try_get_jwt_user_data
from models.jwt import JWTUserData
from typing import List, Union, Optional

router = APIRouter(
    tags=["Properties - CRUD Endpoints"],
    prefix="/api"
)


@router.post("/properties/")
def create_property(
    new_property: PropertyIn,
    queries: PropertyRepository = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
) -> Optional[Union[PropertyOut, dict]]:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to create a property."
        )
    try:
        if new_property.tenant == 0 or new_property.tenant is None:
            new_property.tenant = None
        if new_property.community == 0 or new_property.community is None:
            new_property.community = None
        newproperty = queries.create(new_property)
        return newproperty
    except Exception as e:
        print(e)
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)


@router.get("/properties/{property_id}/")
def get_property(
    property_id: int,
    response: Response,
    repo: PropertyRepository = Depends(),
) -> Union[PropertyOut, dict, Error]:
    try:
        propertea = repo.get_property_by_id(property_id)
        if propertea is None:
            response.status_code = 404
            return {
                "message": "Could not find a property matching that id."
            }
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Failed to fetch that property."}
    return propertea


@router.get("/properties/")
def get_all_properties(
    response: Response,
    repo: PropertyRepository = Depends(),
) -> Optional[Union[List[PropertyOut], dict]]:
    try:
        return repo.get_properties()
    except Exception as e:
        print(e)
        response.status_code = 400
        return {
            "message": "Failed to fetch properties."
        }


@router.delete("/properties/{property_id}/")
def delete_property(
    property_id: int,
    response: Response,
    repo: PropertyRepository = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to delete this property."
        )
    try:
        return repo.delete_property(property_id)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Failed to delete the property."}


@router.put("/properties/{property_id}/")
def update_property(
    property_id: int,
    property_update: PropertyEdit,
    response: Response,
    repo: PropertyRepository = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
) -> Optional[Union[PropertyOut, dict]]:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to update this property."
        )
    try:
        propertea = repo.get_property_by_id(property_id)
        if propertea is None:
            response.status_code = 404
            return {
                "message": "Failed to find a property with that id."
            }
        return repo.update(property_id, property_update)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {
            "message": "Failed to update property."
        }
