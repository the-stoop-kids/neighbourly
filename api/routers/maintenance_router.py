from fastapi import (  # type: ignore
    Depends,
    Response,
    HTTPException,
    status,
    APIRouter,
)

from models.maintenance import (
    MaintenanceIn,
    MaintenanceOut,
    MaintenanceEdit,
    FEMaintenanceOut,
    SimpleMaintenanceOut,
)

from models.errors import Error
from utils.exceptions import UserDatabaseException
from queries.maintenance_queries import MaintenanceQueries
from utils.authentication import try_get_jwt_user_data
from models.jwt import JWTUserData
from typing import List, Optional, Union

router = APIRouter(tags=["Maintenance - CRUD Endpoints"], prefix="/api")


@router.post("/maintenance/")
def create_request(
    new_request: MaintenanceIn,
    repo: MaintenanceQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
) -> SimpleMaintenanceOut:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to create a maintenance ticket."
        )
    try:
        return repo.create_maintenance_request(new_request)
    except UserDatabaseException as e:
        print(e)
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)


@router.get("/maintenance/")
def all_requests(
    response: Response,
    maintenance: MaintenanceQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
) -> Optional[List[MaintenanceOut]]:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to view maintenance tickets."
        )
    try:
        return maintenance.get_requests()
    except Exception as e:
        print(e)
        response.status_code = 400
        return {
            "message": "Failed to fetch maintenance tickets."
        }


@router.get("/maintenance/{maintenance_id}/")
def get_request_by_id(
    maintenance_id: int,
    response: Response,
    repo: MaintenanceQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
) -> Union[FEMaintenanceOut, dict, Error]:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to view that maintenance ticket."
        )
    try:
        maintenance = repo.get_request_by_id(maintenance_id)
        if maintenance is None:
            response.status_code = 404
            return {
                "message": "Could not find a maintenance ticket matching id."
            }
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Failed to fetch the maintenance ticket."}
    return maintenance


@router.put("/maintenance/{maintenance_id}/")
def update_request(
    maintenance_id: int,
    maintenance_update: MaintenanceEdit,
    response: Response,
    repo: MaintenanceQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
) -> Optional[Union[SimpleMaintenanceOut, dict]]:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to update this maintenance ticket."
        )
    try:
        maintenance = repo.get_request_by_id(maintenance_id)
        if maintenance is None:
            response.status_code = 404
            return {
                "message": "Failed to find a maintenance ticket with that id."
            }
        return repo.update(maintenance_id, maintenance_update)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {
            "message": "Failed to update maintenance."
        }


@router.delete("/maintenance/{maintenance_id}/")
def delete_request(
    maintenance_id: int,
    response: Response,
    repo: MaintenanceQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to delete this maintenance ticket."
        )
    try:
        return repo.delete(maintenance_id)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Failed to delete the maintenance."}
