from queries.review_queries import ReviewRepository
from typing import List, Union, Optional
from fastapi import (  # type: ignore
    Depends,
    Response,
    HTTPException,
    status,
    APIRouter,
)
from models.reviews import (
    FrontEndReviewOut,
    ReviewIn,
    ReviewEdit,
    ReviewOut,
)
from models.errors import Error
from utils.exceptions import UserDatabaseException
from utils.authentication import try_get_jwt_user_data
from models.jwt import JWTUserData

router = APIRouter(
    tags=["Review - CRUD Endpoints"],
    prefix="/api"
)


@router.post("/reviews/")
def create_review(
    review: ReviewIn,
    response: Response,
    repo: ReviewRepository = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
) -> Optional[Union[ReviewOut, dict]]:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to create a review."
        )
    try:
        if review.property_id is None or review.property_id == 0:
            review.property_id = None
        return repo.create(review)
    except UserDatabaseException as e:
        print(e)
        response.status_code = 400
        return {"message": "Failed to create a review."}


@router.get("/reviews/{review_id}/")
def get_review(
    review_id: int,
    response: Response,
    repo: ReviewRepository = Depends()
) -> Union[ReviewOut, dict, Error]:
    try:
        review = repo.get_review_by_id(review_id)
        if review is None:
            response.status_code = 404
            return {
                "message": "Could not find a review matching that ID"
            }
    except Exception as e:
        print(e)
        response.status_code = 400
        message = "Failed to fetch the review."
        return {"message": message}
    return review


@router.get("/reviews/")
def get_reviews(
    response: Response,
    repo: ReviewRepository = Depends()
) -> Optional[Union[List[FrontEndReviewOut], dict]]:
    try:
        return repo.get_all_reviews()
    except Exception as e:
        print(e)
        response.status_code = 400
        return {
            "message": "Failed to fetch reviews."
        }


@router.put("/reviews/{review_id}/")
def update_review(
    review_id: int,
    review_update: ReviewEdit,
    response: Response,
    repo: ReviewRepository = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
) -> Optional[Union[ReviewOut, dict]]:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to update your review."
        )
    try:
        review = repo.get_review_by_id(review_id)
        if review is None:
            response.status_code = 404
            return {"message": "Failed to find a review with that id."}
        return repo.update(review_id, review_update)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Failed to update review."}


@router.delete("/reviews/{review_id}/")
def delete_review(
    review_id: int,
    response: Response,
    repo: ReviewRepository = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to delete your review."
        )
    try:
        return repo.delete(review_id)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Failed to delete the review."}
