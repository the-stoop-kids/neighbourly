from fastapi import (  # type: ignore
    Depends,
    Response,
    APIRouter,
    HTTPException,
    status,
)
from queries.user_queries import (
    UserQueries,
)
from models.users import (
    UserOut,
    UserEdit,
    UserPassword,
)
from utils.authentication import (
    hash_password,
)
from utils.authentication import try_get_jwt_user_data
from models.jwt import JWTUserData
from typing import Union, Optional

router = APIRouter(tags=["Users - CRUD Endpoints"], prefix="/api")


@router.delete("/users/{user_id}/")
def delete_user(
    id: int,
    response: Response,
    repo: UserQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to delete your account."
        )
    try:
        return repo.delete(id)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Failed to delete the user."}


@router.put("/users/{user_id}/")
def update_user(
    user_id: int,
    user_update: UserEdit,
    repo: UserQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
) -> Optional[UserOut]:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to update your account."
        )
    try:
        user = repo.update_user(user_id, user_update)
        return user
    except Exception as e:
        print(e)


@router.get("/users/")
def get_all(
    response: Response,
    repo: UserQueries = Depends(),
):
    try:
        return repo.get_all_users()
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Failed to fetch all users."}


@router.get("/users/{user_id}/")
def get_user_by_id(
    user_id: int,
    repo: UserQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
) -> Optional[Union[UserOut, dict]]:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to view this profile."
        )
    try:
        user = repo.get_by_id(user_id)
        return user
    except Exception as e:
        print(e)


@router.put("/users/{user_id}/reset-password/")
def password_reset(
    user_id: int,
    update_user: UserPassword,
    repo: UserQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
) -> Optional[Union[UserOut, dict]]:

    if update_user.password != update_user.password2:
        return {"message": "Passwords do not match"}

    hashed_password = hash_password(update_user.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to reset the password on your account."
        )
    try:
        user = repo.reset_password(user_id, hashed_password)
        return user

    except Exception as e:
        print(e)
