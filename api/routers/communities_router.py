from fastapi import (  # type: ignore
    Depends,
    Response,
    HTTPException,
    status,
    APIRouter,
)

from models.communities import (
    CommunitiesIn,
    CommunitiesOut,
    CommunitiesEdit,
    GetCommunitiesOut,
)

from models.errors import Error
from utils.exceptions import UserDatabaseException
from queries.communities_queries import CommunityQueries
from utils.authentication import try_get_jwt_user_data
from models.jwt import JWTUserData
from typing import List, Optional, Union

router = APIRouter(tags=["Communities - CRUD Endpoints"], prefix="/api")


@router.post("/communities/")
def create_community(
    new_community: CommunitiesIn,
    queries: CommunityQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
) -> CommunitiesOut:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to create a community."
        )
    try:
        return queries.create_community(new_community)
    except UserDatabaseException as e:
        print(e)
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)


@router.get("/communities/")
def all_communities(
    response: Response,
    community: CommunityQueries = Depends(),
) -> Optional[Union[List[GetCommunitiesOut], dict]]:
    try:
        return community.get_communities()
    except UserDatabaseException as e:
        print(e)
        response.status_code = 400
        return {
            "message": "Failed to fetch communities."
        }


@router.get("/communities/{community_id}/")
def get_community(
    community_id: int,
    response: Response,
    repo: CommunityQueries = Depends()
) -> Union[CommunitiesOut, dict, Error]:
    try:
        community = repo.get_community_by_id(community_id)
        if community is None:
            response.status_code = 404
            return {
                "message": "Could not find a community matching that id."
            }
    except Exception as e:
        print(e)
        response.status_code = 400
        message = "Failed to fetch the community."
        return {"message": message}
    return community


@router.put("/communities/{community_id}/")
def update_community(
    community_id: int,
    community_update: CommunitiesEdit,
    response: Response,
    repo: CommunityQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
) -> Optional[Union[CommunitiesOut, dict]]:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to update this community."
        )
    try:
        community = repo.get_community_by_id(community_id)
        if community is None:
            response.status_code = 404
            return {
                "message": "Failed to find a community with that id."
            }
        return repo.update(community_id, community_update)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {
            "message": "Failed to update community."
        }


@router.delete("/communities/{community_id}/")
def delete_community(
    community_id: int,
    response: Response,
    repo: CommunityQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please sign in to delete this community."
        )
    try:
        return repo.delete(community_id)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Failed to delete the community."}
