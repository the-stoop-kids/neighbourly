from fastapi import (  # type: ignore
    Depends,
    Response,
    HTTPException,
    status,
    APIRouter,
)

from models.events import (
    EventIn,
    EventOut,
    EventEdit,
    GetEventOut,
)

from models.errors import Error
from utils.exceptions import UserDatabaseException
from queries.event_queries import EventQueries
from typing import List, Union, Optional

router = APIRouter(
    tags=["Events - CRUD Endpoints"],
    prefix="/api"
)


@router.post("/events/")
def create_event(
    new_event: EventIn,
    queries: EventQueries = Depends(),
) -> EventOut:
    try:
        event = queries.create(new_event)
        return event
    except UserDatabaseException as e:
        print(e)
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)


@router.get("/events/")
def all_events(
    events: EventQueries = Depends()
) -> List[GetEventOut]:
    try:
        return events.get_all_events()
    except UserDatabaseException as e:
        print(e)
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)


@router.get("/events/{event_id}/")
def get_event(
    event_id: int,
    response: Response,
    repo: EventQueries = Depends(),
) -> Union[EventOut, dict, Error]:
    try:
        event = repo.get_event_by_id(event_id)
        if event is None:
            response.status_code = 404
            return {
                "message": "Could not find an event matching that ID."
            }
    except Exception as e:
        print(e)
        response.status_code = 400
        message = "Failed to fetch event."
        return {"message": message}
    return event


@router.put("/events/{event_id}/")
def update_event(
    event_id: int,
    event_update: EventEdit,
    response: Response,
    repo: EventQueries = Depends(),
) -> Optional[Union[EventOut, dict]]:
    try:
        event = repo.get_event_by_id(event_id)
        if event is None:
            response.status_code = 404
            return {
                "message": "Failed to find an event with that ID."
            }
        return repo.update(event_id, event_update)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {
            "message": "Failed to update event."
        }


@router.delete("/events/{event_id}/")
def delete_event(
    event_id: int,
    response: Response,
    repo: EventQueries = Depends(),
):
    try:
        return repo.delete(event_id)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Failed to delete the event."}
