from models.jwt import JWTUserData
from utils.authentication import try_get_jwt_user_data
from queries.bill_queries import BillRepository
from typing import List, Union, Optional
from fastapi import (  # type: ignore
    Depends,
    Response,
    APIRouter,
    HTTPException,
    status,
)
from models.bills import (
    BillIn,
    BillOut,
)
from models.errors import Error

router = APIRouter(
    tags=["Bills - CRUD Endpoints"],
    prefix="/api"
)


@router.post("/bills/")
async def create_bill(
    bill: BillIn,
    response: Response,
    repo: BillRepository = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data)
) -> Optional[Union[BillOut, dict]]:

    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You need to be a user to create a bill."
        )
    try:
        return repo.create_bill(bill)

    except Exception as e:
        print(e)
        response.status_code = 400
        message = "Failed to create a bill."
        return {"message": message}


@router.get("/bills/{bill_id}/")
def get_bill(
    bill_id: int,
    response: Response,
    repo: BillRepository = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data)
) -> Union[BillOut, dict, Error]:

    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You need to be a user to see this bill."
        )
    try:
        bill = repo.get_bill_by_id(bill_id)
        if bill is None:
            response.status_code = 404
            return {
                "message": "Could not find a bill matching that ID"
            }
    except Exception as e:
        print(e)
        response.status_code = 400
        message = "Failed to fetch the bill."
        return {"message": message}
    return bill


@router.get("/bills/")
def get_bills(
    response: Response,
    repo: BillRepository = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data)
) -> Optional[Union[List[BillOut], dict]]:

    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You need to be a user to view bills."
        )
    try:
        return repo.get_all_bills()
    except Exception as e:
        print(e)
        response.status_code = 400
        return {
            "message": "Failed to fetch bills."
        }


@router.put("/bills/{bill_id}/")
def update_bill(
    bill_id: int,
    bill_update: BillIn,
    response: Response,
    repo: BillRepository = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data)
) -> Optional[Union[BillOut, dict]]:

    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You need to be a user to update a bill."
        )
    try:
        bill = repo.get_bill_by_id(bill_id)
        if bill is None:
            response.status_code = 404
            return {
                "message": "Failed to find a bill with that id."
            }
        return repo.update(bill_id, bill_update)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {
            "message": "Failed to update bill."
        }


@router.delete("/bills/{bill_id}/")
def delete_bill(
    bill_id: int,
    response: Response,
    repo: BillRepository = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data)
):

    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You need to be a user to delete a bill."
        )
    try:
        return repo.delete(bill_id)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Failed to delete the bill."}
