steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE properties (
            id SERIAL PRIMARY KEY NOT NULL,
            street VARCHAR(100) NOT NULL,
            city VARCHAR(50) NOT NULL,
            state VARCHAR(50) NOT NULL,
            zipcode INT NOT NULL,
            beds INT NOT NULL,
            baths FLOAT NOT NULL,
            property_manager INT NOT NULL REFERENCES users(id),
            tenant INT REFERENCES users(id) DEFAULT NULL,
            community INT REFERENCES communities(id) DEFAULT NULL,
            created_on DATE NOT NULL DEFAULT CURRENT_DATE
        );
        CREATE TRIGGER set_timestamp
        BEFORE INSERT ON properties
        FOR EACH ROW
        EXECUTE PROCEDURE trigger_set_timestamp();
        """,
        # "Down" SQL statement
        """
        DROP TABLE properties;
        """
    ],
]
