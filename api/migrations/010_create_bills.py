steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE bills (
            id SERIAL PRIMARY KEY NOT NULL,
            tenant INT NOT NULL REFERENCES users(id),
            property_manager INT NOT NULL REFERENCES users(id),
            link TEXT NOT NULL,
            bill_name VARCHAR(50) NOT NULL,
            bill_type VARCHAR(50) NOT NULL,
            due_date DATE NOT NULL,
            amount DECIMAL(10,2) NOT NULL DEFAULT 0.00,
            complete BOOL DEFAULT FALSE,
            property_id INT REFERENCES properties(id),
            created_on TIMESTAMPTZ NOT NULL DEFAULT NOW()
        );
        CREATE TRIGGER set_timestamp
        BEFORE INSERT ON bills
        FOR EACH ROW
        EXECUTE PROCEDURE trigger_set_timestamp();
        """,
        # "Down" SQL statement
        """
        DROP TABLE bills;
        """
    ],
]
