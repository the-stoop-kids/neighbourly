steps = [
    [
        """
        CREATE OR REPLACE FUNCTION trigger_set_timestamp()
        RETURNS TRIGGER AS $$
        BEGIN
            NEW.created_on = NOW();
        RETURN NEW;
        END;
        $$ LANGUAGE plpgsql;
        """,
        """
        DROP FUNCTION trigger_set_timestamp();
        """
    ],
]
