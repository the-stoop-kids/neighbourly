steps = [
    [
        """
        CREATE TABLE status (
            id SERIAL PRIMARY KEY NOT NULL,
            status VARCHAR(100)
        );
        INSERT INTO status
            (id, status)
        VALUES
            (1, 'Pending'),
            (2, 'Approved'),
            (3, 'Denied'),
            (4, 'In Progress'),
            (5, 'Complete');
        """,
        """
        DROP TABLE status;
        """
    ],
]
