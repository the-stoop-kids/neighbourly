steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE events (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR(100) NOT NULL,
            description VARCHAR(500) NOT NULL,
            location VARCHAR(50) NOT NULL,
            event_date DATE NOT NULL,
            maker INT NOT NULL REFERENCES users(id),
            category INT NOT NULL REFERENCES category (id),
            created_on TIMESTAMPTZ NOT NULL DEFAULT NOW()
        );
        CREATE TRIGGER set_timestamp
        BEFORE UPDATE ON events
        FOR EACH ROW
        EXECUTE PROCEDURE trigger_set_timestamp();
        """,
        # "Down" SQL statement
        """
        DROP TABLE events;
        """
    ],
]
