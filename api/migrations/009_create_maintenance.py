steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE maintenance (
            id SERIAL NOT NULL PRIMARY KEY,
            issue VARCHAR(500) NOT NULL,
            attempts VARCHAR(1000) NOT NULL,
            outcome VARCHAR(1000),
            assigned_personnel VARCHAR(100),
            status INT NOT NULL REFERENCES status(id) DEFAULT 1,
            resolved BOOL DEFAULT FALSE,
            resolved_on DATE,
            maker INT NOT NULL REFERENCES users(id),
            property_id INT NOT NULL REFERENCES properties(id),
            created_on DATE NOT NULL DEFAULT CURRENT_DATE
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE maintenance;
        """
    ],
]
