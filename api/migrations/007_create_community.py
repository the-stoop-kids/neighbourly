steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE communities (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR(100) NOT NULL,
            owner INT NOT NULL REFERENCES users(id),
            description VARCHAR(500) NOT NULL,
            city VARCHAR(50) NOT NULL,
            state VARCHAR(50) NOT NULL,
            zipcode INT NOT NULL,
            rating INT,
            created_on DATE NOT NULL DEFAULT CURRENT_DATE
        );
        CREATE TRIGGER set_timestamp
        BEFORE INSERT ON communities
        FOR EACH ROW
        EXECUTE PROCEDURE trigger_set_timestamp();
        """,
        # "Down" SQL statement
        """
        DROP TABLE communities;
        """
    ],
]
