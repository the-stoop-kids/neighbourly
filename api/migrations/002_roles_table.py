steps = [
    [
        """
        CREATE TABLE roles (
            id SERIAL PRIMARY KEY NOT NULL,
            role VARCHAR(100)
        );
        INSERT INTO roles
            (id, role)
        VALUES
            (1, 'Property Manager'),
            (2, 'Tenant');
        """,
        """
        DROP TABLE roles;
        """
    ],
]
