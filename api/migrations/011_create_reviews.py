steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE reviews (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR(50) NOT NULL,
            experience VARCHAR(500) NOT NULL,
            reviewer INT NOT NULL REFERENCES users(id),
            rating INT NOT NULL,
            property_id INT REFERENCES properties(id),
            created_on DATE NOT NULL DEFAULT CURRENT_DATE
        );
        CREATE TRIGGER set_timestamp
        BEFORE INSERT ON reviews
        FOR EACH ROW
        EXECUTE PROCEDURE trigger_set_timestamp();
        """,
        # "Down" SQL statement
        """
        DROP TABLE reviews;
        """
    ],
]
