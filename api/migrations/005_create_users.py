steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY NOT NULL,
            username VARCHAR(100) NOT NULL UNIQUE,
            hashed_password VARCHAR(256) NOT NULL,
            first_name VARCHAR(50) NOT NULL,
            last_name VARCHAR(50) NOT NULL,
            picture_url TEXT,
            email VARCHAR(100) NOT NULL,
            phone_number BIGINT NOT NULL,
            role INT NOT NULL REFERENCES roles(id),
            created_on TIMESTAMPTZ NOT NULL DEFAULT NOW()
        );
        CREATE TRIGGER set_timestamp
        BEFORE INSERT ON users
        FOR EACH ROW
        EXECUTE PROCEDURE trigger_set_timestamp();
        """,
        # "Down" SQL statement
        """
        DROP TABLE users;
        """
    ],
]
