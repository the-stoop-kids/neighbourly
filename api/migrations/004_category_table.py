steps = [
    [
        """
        CREATE TABLE category (
            id SERIAL PRIMARY KEY NOT NULL,
            category VARCHAR(100)
        );
        INSERT INTO category
            (id, category)
        VALUES
            (1, 'Activity/Recreation'),
            (2, 'Announcement'),
            (3, 'Buy/Sell'),
            (4, 'Charity Event'),
            (5, 'Conference'),
            (6, 'Entertainment'),
            (7, 'Exhibit'),
            (8, 'Food/Drink'),
            (9, 'Local Gathering'),
            (10, 'Meeting'),
            (11, 'Networking'),
            (12, 'Performance'),
            (13, 'Workshop/Seminar'),
            (14, 'Virtual Event'),
            (15, 'Other (Please specify in title)');
        """,
        """
        DROP TABLE category;
        """
    ],
]
