"""
Entry point for the FastAPI Application
"""
import os
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import (
    auth_router,
    event_router,
    bill_router,
    review_router,
    property_router,
    communities_router,
    maintenance_router,
    user_router,
)


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST", "http://localhost:5173")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(auth_router.router)
app.include_router(event_router.router)
app.include_router(bill_router.router)
app.include_router(review_router.router)
app.include_router(communities_router.router)
app.include_router(maintenance_router.router)
app.include_router(property_router.router)
app.include_router(user_router.router)
