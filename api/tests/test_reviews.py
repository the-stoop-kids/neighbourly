from main import app
from fastapi.testclient import TestClient
from queries.review_queries import ReviewRepository
from models.reviews import ReviewIn, ReviewOut
from datetime import datetime


client = TestClient(app)


class EmptyGetReviewQueries:
    def get_all_reviews(self):
        return []

    # def get_review_by_id(self):
    #     return {}


class PostReviewQueries:
    def create(self, review: ReviewIn) -> ReviewOut:
        return ReviewOut(
            id=1,
            title=review.title,
            experience=review.experience,
            reviewer=review.reviewer,
            rating=review.rating,
            property_id=review.property_id,
            created_on=datetime(2024, 6, 18),
        )

# class PutReviewQueries:
#     def update_review(self, review):
#         result = {
#             "id": 1,
#             "title": "Testing the update review for testing",
#             "experience": "I'm sure it's going to suck",
#             "reviewer": 1,
#             "rating": 1,
#             "property_id": 1,
#             "created_on": "2024-06-18T00:00:00"
#         }
#         result.update(review)
#         return result

# class DeleteReviewQuery:
#     def delete_review(self, review):


def test_get_all_reviews():
    # ARRANGE
    app.dependency_overrides[ReviewRepository] = EmptyGetReviewQueries
    response = client.get("/api/reviews/")

    # ACT
    app.dependency_overrides = {}

    # ASSERT
    assert response.status_code == 200
    assert response.json() == []


def test_create_review():
    json_in = {
        "title": "Testing the update review for testing",
        "experience": "I'm sure it's going to suck",
        "reviewer": 1,
        "rating": 1,
        "property_id": 1
    }

    # ARRANGE
    app.dependency_overrides[ReviewRepository] = PostReviewQueries

    # ACT
    response = client.post("/api/reviews/", json=json_in)

    # ASSERT
    assert response.status_code == 401

    # CLEAN UP
    app.dependency_overrides = {}
