from datetime import datetime
from fastapi.testclient import TestClient
from main import app
from models.events import EventIn, EventOut
from queries.event_queries import EventQueries


client = TestClient(app)


class EmptyEventsQueries:
    def get_all_events(self):
        return []


class ExampleEventsQueries(EventQueries):
    def create(self, event: EventIn) -> EventOut:
        return EventOut(
            id=1,
            title=event.title,
            description=event.description,
            location=event.location,
            event_date=event.event_date,
            maker=event.maker,
            category=event.category,
            created_on=datetime(2010, 5, 5),
        )


def test_get_all_events():
    # ARRANGE
    app.dependency_overrides[EventQueries] = EmptyEventsQueries

    # ACT
    response = client.get("/api/events/")
    app.dependency_overrides = {}

    # ASSERT
    assert response.status_code == 200


def test_create_event():
    # ARRANGE
    app.dependency_overrides[EventQueries] = ExampleEventsQueries

    example_event = {
        "title": "Test Title",
        "description": "This is a test",
        "location": "Test Location",
        "event_date": "2024-08-01",
        "maker": 1,
        "category": 1,
    }

    # ACT
    response = client.post("/api/events/", json=example_event)

    # ASSERT
    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        **example_event,
        "created_on": '2010-05-05T00:00:00'
    }

    # CLEAN UP
    app.dependency_overrides = {}
