from datetime import datetime
from fastapi.testclient import TestClient
from main import app
from models.communities import CommunitiesIn, CommunitiesOut
from queries.communities_queries import CommunityQueries

client = TestClient(app)


class EmptyCommunityQueries:
    def get_communities(self):
        return []


class ExampleCommunityQueries(CommunityQueries):
    def create_community(self, community: CommunitiesIn) -> CommunitiesOut:
        return CommunitiesOut(
            id=1,
            title=community.title,
            owner=community.owner,
            description=community.description,
            city=community.city,
            state=community.state,
            zipcode=community.zipcode,
            rating=community.rating,
            created_on=datetime(2010, 5, 5),
        )


def test_get_all_communities():
    app.dependency_overrides[CommunityQueries] = EmptyCommunityQueries

    response = client.get("/api/communities/")
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == []


def test_create_community():
    app.dependency_overrides[CommunityQueries] = ExampleCommunityQueries

    example_community = {
        "title": "Test Community",
        "owner": 1,
        "description": "This is a test community",
        "city": "Test City",
        "state": "Test State",
        "zipcode": "12345",
        "rating": 5,
    }

    response = client.post("/api/communities/", json=example_community)

    assert response.status_code == 401
    assert response.json() == {
        "detail": "Please sign in to create a community."
    }

    app.dependency_overrides = {}
    client.cookies.clear()


def test_create_community_unauthorized():

    app.dependency_overrides[CommunityQueries] = EmptyCommunityQueries

    example_community = {
        "title": "test_community",
        "owner": 1,
        "description": "this is a test community",
        "city": "Test City",
        "state": "Test State",
        "zipcode": "12345",
        "rating": 5,
    }

    response = client.post("/api/communities/", json=example_community)
    assert response.status_code == 401
    app.dependency_overrides = {}
