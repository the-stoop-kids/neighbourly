# Neighbourly

placeholder

## Project Team - The Stoop Kids

-   **Erika Linden**
-   **Jaron Laquindanum**
-   **Benjamin Stults**

## Design

-   [API](./docs/API.md)
-   [Data Schema](./docs/DATASCHEMA.md)
-   [Wireframes](./docs.WIREFRAMES.md)
-   [Team Contribution Breakdown](./docs/Contributions.md)

## Intended Market

placeholder

## Functionality

### Property Managers

-   Property Managers are users where role is equal to 1.
-   Property Managers can view every page on the app.
-   Property Managers can create, update, and delete event that they created.
-   Property Managers can create, update, and delete a community that they created.
-   Property Managers can view property page.
-   Property Managers can create, update, and delete a review that they created.
-   Property Managers can update (approve) maintenance requests.
-   Property Managers can create, update, and delete bills that they created.
-   Property Managers can view their user page, update their data, and reset their passwords.
-   Property Managers can view the directory.

### Tenants

-   Tenants are users where role is equal to 2.
-   Tenants can view every page on the app.
-   Tenants can create, update, and delete an event that they created.
-   Tenants can create, update, and delete a community that they created.
-   Tenants can create, update, and delete a property that they created.
-   Tenants can create, update, and delete a review that they created.
-   Tenants can create and update maintenance requests.
-   Tenants can view bills page.
-   Tenants can view their user page, update their data, and reset their passwords.
-   Tenants can view the directory.

### Visitors

-   Visitors (not logged in users) can view the home page, events page, communities, properties, reviews
-   Vistors can signup and log in

## Project Setup and Configuration:

1. Clone the repository with HTTPS in your terminal:

```
git clone https://gitlab.com/the-stoop-kids/neighbourly.git
```

2. Switch into neighborly by double-clicking the directory or:

```
cd neighbourly
```

3. In the directory (or in VSCode using 'code .' in the terminal), copy and paste a copy of the file `env.sample` and rename it to `.env`.

4. Open docker desktop to start building the required image and containers.

5. In your terminal, ensure you are still in the neighbourly directory and type:

```
docker compose build
```

6. Run docker containers using the image you just built:
    - -d is optional: it runs docker in detached mode

```
docker compose up -d
```

7. Verify that 3 containers spin up and don't exit early.

8. You can view the app at the following urls:
    - web docs: http://localhost:8000/docs#/
    - web application: http://localhost:5173/
