# Neighbourly:

- Placeholder

## Table of Contents

- [Neighbourly](#neighbourly)
  - [The Stoop Kids](#the-stoop-kids)
  - [Setup Instructions](#how-to-run-this-application)
  - [Design](#diagram)
	- [API/Endpoint Documentation](#api-and-endpoint-documentation)
		- [URLs and Ports](#urls-and-ports)
		- [User Table and Endpoints](#user-table-and-endpoints)
			- [Example Response/Request Data](#example-responserequest-data)
		- [Event Table and Endpoints](#events-table-and-endpoints)
			- [Example Response/Request Data](#example-responserequest-data-1)
		- [Maintenance Table and Endpoints](#maintenance-table-and-endpoints)
			- [Example Response/Request Data](#example-responserequest-data-2)
		- [Property Table and Endpoints](#property-table-and-endpoints)
			- [Example Response/Request Data](#example-responserequest-data-3)
		- [Bill Table and Endpoints](#bills-table-and-endpoints)
			- [Example Response/Request Data](#example-responserequest-data-4)
		- [Review Table and Endpoints](#review-table-and-endpoints)
			- [Example Response/Request Data](#example-responserequest-data-5)
        - [Community Table and Endpoints](#community-table-and-endpoints)
            - [Example Response/Request Data](#example-responserequest-data-6)


## The Stoop Kids:

* **Erika Linden**
* **Jaron Laquindanum**
* **Benjamin Stults**

## How to Run this Application:

- Placeholder

## API and Endpoint Documentation:

### URLS and PORTS:

| API | PORT | URL
| ----------- | ----------- | ----------- |
| FastAPI Docs | 8000 | http://localhost:8000/docs#
| Neighbourly | 5173| http://localhost:5173

#### User Table and Endpoints:

| ACTION | METHOD | URL
| ----------- | ----------- | ----------- |
User List | GET | http://localhost:8000/api/users/
Create User | POST | http://localhost:8000/api/users/
User Detail | GET | http://localhost:8000/api/users/<<int:id>>/
Update User | PUT | http://localhost:8000/api/users/<<int:id>>/
Delete User | DELETE | http://localhost:8000/api/users/<<int:id>>/

#### Example Response/Request Data:

JSON Body to send data:

- Create (POST) a User:

```
{
    "first_name": varchar(str),
    "last_name:" varchar(str),
    "phone_number": integer(max length 10),
    "username": varchar(str),
    "picture_url": HttpUrl(url field),
    "password": varchar(str),
    "email": varchar(str),
    "role": integer(only "0" or "1") [possible primary key],
    "property_id": integer - [primary key],
    "created_on": date (auto)
}
```

- Update (PUT) a User by <<int:id>>:

```
{
    "username": varchar(str),
    "picture_url": HttpUrl(url field),
    "password": varchar(str),
    "phone_number": integer(max length 10),
    "email": varchar(str)
}
```

JSON Body returned Data:

- List (GET) Users or (GET) a User's details by <<int:id>>:

```
{
    "users": [
        {
            "id": integer [primary key],
            "first_name": varchar(str),
            "last_name": varchar(str),
            "email": varchar(str),
            "phone_number": integer(max length 10),
            "username": varchar(str),
            "password": varchar(str),
            "role": integer(only "0" or "1") [possible primary key],
            "property_id": {
                "id": integer - Listing [primary key],
                "street": varchar(str),
                "city": varchar(str),
                "state": varchar(str),
                "zipcode": integer(5),
                "property_manager": {
                    "id": integer - User [primary key],
                    "first_name": varchar(str),
                    "last_name": varchar(str),
                    "email": varchar(str),
                    "phone_number": integer(max length 10),
                    "username": varchar(str),
                    "created_on": date (auto)
                },
                "tenant": {
                    "id": integer - User [primary key],
                    "first_name": varchar(str),
                    "last_name": varchar(str),
                    "email": varchar(str),
                    "created_on": date (auto),
                    "phone_number": integer(max length 10),
                    "username": varchar(str),
                    "created_on": date (auto)
                },
            "created_on": date (auto)
            },
        }
    ]
}
```

- Delete a User by <<int:id>>:

```
{
	"deleted": True
}
```

#### EVENTS Table and Endpoints:

| ACTION | METHOD | URL
| ----------- | ----------- | ----------- |
| List events | GET | http://localhost:8000/api/events/
| Create an event | POST | http://localhost:8000/api/events/
| Get a specific event | GET | http://localhost:8000/api/events/<<int:id>>/
| Update a specific event | PUT | http://localhost:8000/api/events/<<int:id>>/
| Delete a specific event | DELETE | http://localhost:8000/api/events/<<int:id>>/

#### EXAMPLE RESPONSE/REQUEST DATA:

JSON Body to send data:

- Create (POST) an event:

```
{
	"title": varchar(str),
	"description": varchar(str),
    "category": varchar(str),
	"location": varchar(str),
	"when": date,
    "created_on": date (auto),
    "maker": user.id
}
```

- Update (PUT) an event:

```
{
	"location": varchar(str),
	"when": date
}
```

JSON Body returned data:

- List (GET) events by maker.id or (GET/PUT) event detail by <<int:id>> (event.id):

```
{
	"events": [
		{
			"id": integer [primary key],
			"title": varchar(str),
			"description": varchar(str),
			"location": varchar(str),
			"when": date,
            "created_on": date (auto),
			"maker": {
                "id": integer - User [primary key],
                "first_name": varchar(str),
                "last_name": varchar(str),
                "email": varchar(str),
                "phone_number": integer(max length 10),
                "role": varchar(str),
                "property_id": {
                    "id": integer [primary key],
                    "street": varchar(str),
                    "city": varchar(str),
                    "state": varchar(str),
                    "zipcode": integer(5),
                    "property_manager": {
                        "id": integer - User [primary key],
                        "first_name": varchar(str),
                        "last_name": varchar(str),
                        "email": varchar(str),
                        "phone_number": integer(max length 10),
                        "username": varchar(str),
                        "created_on": date (auto)
                    },
                    "tenant": {
                        "id": integer - User [primary key],
                        "first_name": varchar(str),
                        "last_name": varchar(str),
                        "email": varchar(str),
                        "phone_number": integer(max length 10),
                        "username": varchar(str),
                        "created_on": date (auto)
                    }
                },
            }
		},
	]
}
```

- Delete an event by <<int:id>>:

```
{
	"deleted": True
}
```


#### MAINTENANCE Table and Endpoints:

| ACTION | METHOD | URL
| ----------- | ----------- | ----------- |
| List maintenance | GET | http://localhost:8000/api/property/<<int:id>>/maintenance/
| Create a maintenance | POST | http://localhost:8000/api/property/<<int:id>>/maintenance/
| Get a specific maintenance | GET | http://localhost:8000/api/property/<<int:id>>/maintenance/<<int:id>>/
| Update a specific maintenance | PUT | http://localhost:8000/api/property/<<int:id>>/maintenance/<<int:id>>/
| Delete a specific maintenance | DELETE | http://localhost:8000/api/property/<<int:id>>/maintenance/<<int:id>>/

#### EXAMPLE RESPONSE/REQUEST DATA:

JSON Body to send data:

- Create (POST) a maintenance item: *tenant

```
{
	"issue": varchar(str),
	"attempts": varchar(str),
    "outcome": varchar(str) (default=null),
	"phone_number": integer(max length 10),
	"assigned_personnel": varchar(str)
    "status": varchar(str) (default="pending"),
    "maker": user.id,
    "created_on": date (auto)
}
```

- Update (PUT) a maintenance item:
    - Tenant View

```
{
	"issues": varchar(str),
	"attempts": varchar(str)
}
```

- Update (PUT) a maintenance item:
    - Property_Manager View

```
{
	"issues": varchar(str),
	"attempts": varchar(str),
	"outcome": varchar(str) (default=null),
	"phone_number": integer(max length 10),
	"assigned_personnel": varchar(str),
    "status": varchar(str) (default="pending"),
	"resolved": bool
}
```

JSON Body returned data:

- List (GET) maintenance items by property.id or (GET/PUT) maintenance item detail by <<int:id>> (maintenance.id):

```
{
	"maintenance": [
		{
			"id": integer [primary key],
			"issue": varchar(str),
			"attempts": varchar(str),
            "outcome": varchar(str) (default=null),
			"assigned_personnel": varchar(str),
            "status": varchar(str) (default="pending"),
			"resolved": bool,
			"resolved_on": date (autocomplete on resolved == True),
			"created_on": date (autocomplete on creation),
			"maker": {
				"id": integer - User [primary key],
                "first_name": varchar(str),
                "last_name": varchar(str),
                "email": varchar(str),
                "phone_number": integer(max length 10),
                "username": varchar(str),
                "created_at": date,
                "role": integer(only "0" or "1") [possible primary key],
				"property_id": {
                    "id": integer - Listing [primary key],
                    "street": varchar(str),
                    "city": varchar(str),
                    "state": varchar(str),
                    "zipcode": integer(5),
                    "property_manager": {
                        "id": integer - User [primary key],
                        "first_name": varchar(str),
                        "last_name": varchar(str),
                        "email": varchar(str),
                        "phone_number": integer(max length 10),
                        "username": varchar(str)
                    },
                    "tenant": {
                        "id": integer - User [primary key],
                        "first_name": varchar(str),
                        "last_name": varchar(str),
                        "email": varchar(str),
                        "phone_number": integer(max length 10),
                        "username": varchar(str)
                    }
                }
			},
		},
	]
}
```

- Delete a maintenance item by <<int:id>>: (NOTE: "id" will be null on success)

```
{
	"deleted": True
}
```

#### PROPERTY Table and Endpoints:

| ACTION | METHOD | URL
| ----------- | ----------- | ----------- |
| List properties | GET | http://localhost:8000/api/communities/<<int:id>>/properties/
| Create a new property | POST | http://localhost:8000/api/communities/<<int:id>>/properties/
| Get details of a property | GET | http://localhost:8000/api/communities/<<int:id>>/properties/<<int:id>>/
| Update a property | PUT | http://localhost:8000/api/communities/<<int:id>>/properties/<<int:id>>/
| Delete a property | DELETE | http://localhost:8000/api/communities/<<int:id>>/properties/<<int:id>>/

#### EXAMPLE RESPONSE/REQUEST DATA:

JSON Body to send data:

- Create (POST) a property:

```
{
    "street": varchar(str),
    "city": varchar(str),
    "state": varchar(str),
    "zipcode": integer(5),
    "beds": integer(2 - integers),
    "baths": float,
    "property_manager": user.id,
    "tenant": user.id - null=True,
    "community": community.id,
    "created_on": date (auto)
}
```

- Update (PUT) a property by <<int:id>>:

```
{
    "property_manager": integer - User [primary key],
    "tenant_id": integer - User [primary key],
    "beds": integer(2 - integers),
    "baths": float,
    "community": community.id
}
```

JSON Body returned data:

- List (GET) properties or (GET/PUT) property detail by <<int:id>> (property.id):

```
{
	"properties": [
		{
            "id": integer - Listing [primary key],
            "created_on": date,
            "street": varchar(str),
            "city": varchar(str),
            "state": varchar(str),
            "zipcode": integer(5),
            "beds": integer(2 - integers),
            "baths": float,
            "property_manager": {
                "id": integer - User [primary key],
                "first_name": varchar(str),
                "last_name": varchar(str),
                "email": varchar(str),
                "phone_number": integer(max length 10),
                "username": varchar(str)
            },
            "tenant": {
                "id": integer - User [primary key],
                "first_name": varchar(str),
                "last_name": varchar(str),
                "email": varchar(str),
                "phone_number": integer(max length 10),
                "username": varchar(str)
            },
            "community": {
                "id": integer - Community [primary key],
                "title": varchar(str),
                "description": varchar(str),
                "city": varchar(str),
                "state": varchar(str),
                "zipcode": integer(5),
                "rating": integer(1-5),
                "created_on": date (autocomplete on creation)
            },
        },
	]
}
```

- Delete a Listing item by <<int:id>>: (NOTE: "id" will be null on success)

```
{
	"deleted": True
}
```

#### BILLS Table and Endpoints

| ACTION | METHOD | URL
| ----------- | ----------- | ----------- |
| List bills | GET | http://localhost:8000/api/users/<<int:id>>/bills/
| POST bill | POST | http://localhost:8000/api/users/<<int:id>>/bills/
| Get bill detail | GET | http://localhost:8000/api/users/<<int:id>>/bills/<<int:id>>/
| Update bill | PUT | http://localhost:8000/api/users/<<int:id>>/bills/<<int:id>>/
| Delete a specific bill | DELETE | http://localhost:8000/api/users/<<int:id>>/bills/<<int:id>>/


#### EXAMPLE RESPONSE/REQUEST DATA

JSON Body to send data:

- Create (POST) a Bill by <<int:id:>>:
    - Property_Manager View

```
{
    "tenant": integer - User [primary key],
    "property_id": integer - Property [primary key],
    "link": HttpUrl(url field),
    "bill_name": varchar(str),
    "bill_type": varchar(str),
    "due_date": date,
    "amount": float(float),
    "complete": bool.
    "created_on": date (autocomplete on creation)
}
```

- Update (PUT) by <<int:id:>>:
    - Property_Manager View
```
{
    "link": HttpUrl(url field),
    "bill_name": varchar(str),
    "bill_type": varchar(str),
    "due_date": date,
    "amount": float(float),
    "complete": bool
}
```

JSON Body returned data:

- List (GET) Bills (tenant.id) or (GET/PUT) Bill detail by <<int:id>> (bill.id):
    -Tenant and Property_Manager View:

```
{
    "bills": [
        {
            "id": integer [primary key],
            "tenant": {
                "id": integer [primary key],
                "first_name": varchar(str),
                "last_name": varchar(str),
                "email": varchar(str),
                "phone_number": integer(max length 10),
                "username": varchar(str),
                "created_at": date,
                "role": integer(only "0" or "1") [possible primary key],
                "property_id": integer - Property [primary key],
                "created_on": date (auto)
            },
            "property_id": {
                "id": integer [primary key],
                "street": varchar(str),
                "city": varchar(str),
                "state": varchar(str),
                "zipcode": integer(5),
                "property_manager": {
                    "id": integer - User [primary key],
                    "first_name": varchar(str),
                    "last_name": varchar(str),
                    "email": varchar(str),
                    "phone_number": integer(max length 10),
                    "username": varchar(str),
                    "created_on": date (auto)
                }
            },
            "link": HttpUrl(url field),
            "bill_name": varchar(str),
            "bill_type": varchar(str),
            "due_date": date,
            "amount": float(float),
            "complete": bool,
            "created_on": date (autocomplete on creation)
	    },
	]
}
```

- Delete a bill by <<int:id>>:
    - Property_Manager View

```
{
	"deleted": True
}
```

#### Review Table and Endpoints:

| ACTION | METHOD | URL
| ----------- | ----------- | ----------- |
List reviews | GET | http://localhost:8000/api/users/<<int:id>>/reviews/
Create a review | POST | http://localhost:8000/api/users/<<int:id>>/reviews/
Review detail | GET | http://localhost:8000/api/users/<<int:id>>/reviews/<<int:id>>/
Update a review| PUT | http://localhost:8000/api/users/<<int:id>>/reviews/<<int:id>>/
Delete a review | DELETE | http://localhost:8000/api/users/<<int:id>>/reviews/<<int:id>>/

** *<<int:id>> in endpoint is tied to the user that is being reviewed*


#### EXAMPLE RESPONSE/REQUEST DATA

JSON Body to send data:

- Create (POST) a review:

```
{
    "title": varchar(str),
    "reviewer": user.id,
    "experience": varchar(str),
    "rating": integer(1-5),
    "property_id": integer - Property [primary key] (optional),
    "created_on": date (autocomplete on creation)
}
```

- Update (PUT) a review by <<int:id>> (user.id - the receiving user):

```
{
    "title": varchar(str),
    "rating": integer(1-5),
    "experience": varchar(str)
}
```

JSON Body returned Data:

- List (GET) reviews or (GET/PUT) review detail by <<int:id>>:

```
{
    "reviews": [
        {
            "id": integer [primary key],
            "title": varchar(str),
            "reviewer": integer - User [primary key],
            "rating": integer(1-5),
            "experience": varchar(str),
            "property_id": {
                "id": integer [primary key],
                "street": varchar(str),
                "city": varchar(str),
                "state": varchar(str),
                "zipcode": integer(5),
                "property_manager": {
                    "id": integer - User [primary key],
                    "first_name": varchar(str),
                    "last_name": varchar(str),
                    "email": varchar(str),
                    "phone_number": integer(max length 10),
                    "username": varchar(str),
                    "created_on": date (auto)
                }
                "tenant": {
                    "id": integer - User [primary key],
                    "first_name": varchar(str),
                    "last_name": varchar(str),
                    "email": varchar(str),
                    "phone_number": integer(max length 10),
                    "username": varchar(str),
                    "created_on": date (auto)
                }
            },
            "created_on": date (autocomplete on creation)
        },
    ]
}
```

- Delete a review by <<int:id>>:

```
{
	"deleted": True
}
```


#### Community Table and Endpoints:

| ACTION | METHOD | URL
| ----------- | ----------- | ----------- |
List communities | GET | http://localhost:8000/api/communities/
Create a community | POST | http://localhost:8000/api/communities/
Community detail | GET | http://localhost:8000/api/communities/<<int:id>>/
Update a community | PUT | http://localhost:8000/api/communities/<<int:id>>/
Delete a community | DELETE | http://localhost:8000/api/communities/<<int:id>>/

#### EXAMPLE RESPONSE/REQUEST DATA

JSON Body to send data:

- Create (POST) a community:

```
{
    "title": varchar(str),
    "description": varchar(str),
    "city": varchar(str),
    "state": varchar(str),
    "zipcode": integer(5),
    "rating": integer(1-5),
    "maker": user.id (property_managers),
    "created_on": date (autocomplete on creation)
}
```

- Update (PUT) a community by <<int:id>> (user.id - the receiving user):

```
{
    "title": varchar(str),
    "description": varchar(str),
    "city": varchar(str),
    "state": varchar(str),
    "zipcode": integer(5),
    "rating": integer(1-5)
}
```

JSON Body returned Data:

- List (GET) community or (GET/PUT) community detail by <<int:id>>:

```
{
    "communities": [
        {
            "id": integer [primary key],
            "title": varchar(str),
            "description": varchar(str),
            "city": varchar(str),
            "state": varchar(str),
            "zipcode": integer(5),
            "rating": integer(1-5),
            "created_on": date (autocomplete on creation)
        },
    ]
}
```

- Delete a community by <<int:id>>:

```
{
	"deleted": True
}
```
